/**
 * 
 */
package proyecto_2_201820_sec_03_team_07;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.IHashTable;
import model.data_structures.LinearProbingHash;
import model.data_structures.Queue;

/**
 * @author luisgomez
 *	Pruebas Unitarias de la estrucutra Linear Probing  (HashTable)
 *
 */
public class LinearProbingHashTest {

	@Test
	public void capacityTest()
	{
		IHashTable<String, Integer> hash = new LinearProbingHash<String, Integer>(5);
		assertEquals("La capacidad debe ser: ", 5, hash.capacity());
	}
	
	@Test
	public void putAndGetTest()
	{
		IHashTable<String, Integer> hash = new LinearProbingHash<String, Integer>(5);
		hash.put("Luis", 19);
		hash.put("Juana", 18);
		hash.put("Camilo", 17);
		hash.put("David", 16);
		hash.put("Alejo", 20);
		hash.put("Camila", 22);
		hash.put("Fernando", 25);
		assertEquals("El elemento con esa llave deberia ser: ", 19, hash.get("Luis").intValue());
		assertEquals("El elemento con esa llave deberia ser: ", 18, hash.get("Juana").intValue());
		assertEquals("El elemento con esa llave deberia ser: ", 17, hash.get("Camilo").intValue());
		assertEquals("El elemento con esa llave deberia ser: ", 16, hash.get("David").intValue());
		assertEquals("El elemento con esa llave deberia ser: ", 20, hash.get("Alejo").intValue());
		assertEquals("El elemento con esa llave deberia ser: ", 22, hash.get("Camila").intValue());
		assertEquals("El elemento con esa llave deberia ser: ", 25, hash.get("Fernando").intValue());
	}
	
	@Test
	public void rehashTest()
	{
		IHashTable<String, Integer> hash = new LinearProbingHash<String, Integer>(5);
		assertEquals("La capacidad debe ser: ", 5, hash.capacity());
		hash.rehash(15);
		assertEquals("La capacidad se tuvo que haber aumentado de 5 a: ", 15, hash.capacity());
	}
	
	@Test
	public void deleteTest()
	{
		IHashTable<String, Integer> hash = new LinearProbingHash<String, Integer>(5);
		hash.put("Luis", 19);
		hash.put("Juana", 18);
		hash.put("Camilo", 17);
		assertEquals("La tabla deberia borrar y retornar: ", 19, hash.delete("Luis").intValue());
		assertEquals("La tabla deberia borrar y retornar: ", 18, hash.delete("Juana").intValue());
		assertEquals("La tabla deberia borrar y retornar: ", 17, hash.delete("Camilo").intValue());
	}

}
