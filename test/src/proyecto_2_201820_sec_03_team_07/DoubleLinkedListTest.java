package proyecto_2_201820_sec_03_team_07;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.IDoubleLinkedList;
import model.data_structures.DoubleLinkedList;

/**
 * Pruebas Unitarias de la lista doblemente encadenada
 * @author luisgomez
 *
 */
	public class DoubleLinkedListTest {

	@Test
	public void testGetSize() {
		IDoubleLinkedList<Integer> lista = new DoubleLinkedList<Integer>();
		lista.add(new Integer(1));
		lista.add(new Integer(2));
		lista.add(new Integer(3));
		lista.add(new Integer(4));
		lista.add(new Integer(5));
		assertEquals("El tamanio de la lista no es la apropiada ", 5, lista.getSize().intValue());
	}

	//IMPORTANTE, RECUERDE QUE EL METODO ADD AGREGA AL COMIENZO DE LA LISTA...
	@Test
	public void testAdd() {
		IDoubleLinkedList<Integer> lista = new DoubleLinkedList<Integer>();
		lista.add(new Integer(1));
		lista.add(new Integer(2));
		lista.add(new Integer(3));
		lista.add(new Integer(4));
		lista.add(new Integer(5));
		lista.add(new Integer(100));
		assertFalse("Si el elemento es null no se esta agregando correctamente", lista.add(null));
		assertEquals("el numero 100 tuvo que haberse agregado", 100, lista.getElement(0).intValue());
		assertTrue("No se esta agregando correctamente", lista.add(new Integer(50)));
	}

	@Test
	public void testGetElement() {
		IDoubleLinkedList<Integer> lista = new DoubleLinkedList<Integer>();
		lista.add(new Integer(1));
		lista.add(new Integer(2));
		lista.add(new Integer(3));
		lista.add(new Integer(4));
		lista.add(new Integer(5));
		assertEquals("No se esta retornando el numero que es", 1, lista.getElement(4).intValue());
	}

	@Test
	public void testClearAll() {
		IDoubleLinkedList<Integer> lista = new DoubleLinkedList<Integer>();
		lista.add(new Integer(1));
		lista.add(new Integer(2));
		lista.add(new Integer(3));
		lista.add(new Integer(4));
		lista.add(new Integer(5));
		lista.clearAll();
		assertEquals("no se esta borrando todo", 0, lista.getSize().intValue());
	}

}
