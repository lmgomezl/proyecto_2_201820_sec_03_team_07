package proyecto_2_201820_sec_03_team_07;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.RedBlackBST;

public class ArbolBinarioRojoNegroTest {

	@Test
	public void tamanoTest()
	{
		RedBlackBST<String, Integer> arbol = new RedBlackBST<>();
		assertEquals("El tama�o del �rbol no es correcto",0,arbol.size());
		assertEquals("El �rbol deber�a estar vacia: ",true,arbol.isEmpty());
		assertEquals("La altura deber�a ser ",-1,arbol.height());
	}
	
	@Test
	public void darMinTest()
	{
		RedBlackBST<String, Integer> arbol = new RedBlackBST<>();
		arbol.put("X", 1);
		arbol.put("B", 1);
		arbol.put("Y", 1);
		arbol.put("A", 1);
		arbol.put("Z", 1);
		assertEquals("El menor deber�a ser ","A",arbol.min());
		
	}
	
	@Test
	public void darMaxTest()
	{
		RedBlackBST<String, Integer> arbol = new RedBlackBST<>();
		arbol.put("X", 1);
		arbol.put("B", 1);
		arbol.put("Y", 1);
		arbol.put("A", 1);
		arbol.put("Z", 1);
		assertEquals("El mayor deber�a ser ","Z",arbol.max());
	}
	
	@Test
	public void contieneTest()
	{
		RedBlackBST<String, Integer> arbol = new RedBlackBST<>();
		arbol.put("X", 1);
		arbol.put("B", 1);
		arbol.put("Y", 1);
		arbol.put("A", 1);
		arbol.put("Z", 1);
		assertEquals("El mayor deber�a contener Y ",true,arbol.contains("Y"));
	}
}
