/**
 * 
 */
package proyecto_2_201820_sec_03_team_07;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Queue;

/**
 * @author luisgomez
 *	Pruebas Unitarias de la estrucutra de cola (Queue)
 *
 */
public class QueueTest {

	@Test
	public void isEmptyTest()
	{
		Queue<Integer> cola = new Queue<Integer>();
		assertTrue("Deberia haber una fila vacia", cola.isEmpty());
	}

	@Test
	public void sizeTest()
	{
		Queue<Integer> cola = new Queue<Integer>();
		cola.enqueue(new Integer(1));
		cola.enqueue(new Integer(2));
		cola.enqueue(new Integer(3));
		cola.enqueue(new Integer(4));
		cola.enqueue(new Integer(5));

		assertEquals("Deberia retornar 5, no esta funcionando", 5, cola.size());

	}

	@Test
	public void enqueueTest()
	{
		Queue<Integer> cola = new Queue<Integer>();
		cola.enqueue(new Integer(1));
		cola.enqueue(new Integer(2));
		cola.enqueue(new Integer(3));
		cola.enqueue(new Integer(4));
		cola.enqueue(new Integer(5));

		assertEquals("El primero deberia ser 1", 1, cola.darPrimero().darElemento().intValue());
		assertEquals("El ultimo deberia ser 5", 5, cola.darUltimo().darElemento().intValue());
	}

	@Test
	public void dequeueTest()
	{
		Queue<Integer> cola = new Queue<Integer>();
		cola.enqueue(new Integer(1));
		cola.enqueue(new Integer(2));
		cola.enqueue(new Integer(3));
		cola.enqueue(new Integer(4));
		cola.enqueue(new Integer(5));

		cola.dequeue();
		cola.dequeue();
		cola.dequeue();

		assertEquals("El primero deberia ser 4", 4, cola.darPrimero().darElemento().intValue());
		assertEquals("El ultimo deberia ser 5", 5, cola.darUltimo().darElemento().intValue());
	}

}
