package api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import controller.Controller.ResultadoCampanna;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.IHashDoubleLinkedList;
import model.data_structures.LinkedListOrdered;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.vo.*;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Actualizar la informacion del sistema con los datos seleccionados por el usuario y generar/actualizar las estructuras de datos necesarias.
	 * Caso Especial: si rutaTrips y rutaStations son la cadena vacia (""), los datos del sistema deben reiniciarse con un conjunto de trips y de estaciones vacios.
	 * @param rutaTrips ruta del archivo de trips que se va a utilizar
	 * @param rutaStations ruta del archivo de stations que se va a utilizar
	 * @param dataBikeRoutes ruta del archivo de cicloruta que se va a utilizar
	 */
	void cargar();

	void loadBikeRoutesJSON(String jsonFile);

	void loadTrips(String pTripsFile);

	void loadStations(String pStationsFile);

	int getTripsSize();

	int getStationsSize();

	int getRoutesSize();

	void calcularMaxMinLatLong();

	ArrayList<VORoute> buscarCiclaRutasSeparateChaining(double longitude, double latitude) throws Exception;

	ArrayList<VORoute> buscarCiclaRutasLinearProbing(double longitude, double latitude);

	/**
     * Generar una cola con los viajes que terminaron en una estaci�n que tiene capacidad n en la fecha de terminaci�n indicada
     * @param n indica la capacidad de la estaci� en la que termin� el viaje
     * @param fechaTerminacion indica la fecha en que se termin� el viaje
     * @return Cola con los viajes que cumplen las condiciones mencionada en el enunciado
     */
    Queue<VOTrip> A1(LocalDate fechaTerminacion,int n);

    /**
     * Buscar los viajes que tienen duraciones similares.
     * @param n duraci�n en minutos de lo que deben durar los viajes del grupo que se quiere identificar
     * @return Lista con los viajes
     */
    IDoubleLinkedList<VOTrip> A2(int n);

    /**
     * Busca los n viajes con los recorridos m�s largos	en una fecha dada
     * @param n n�mero de viajes que se desea consultar
     * @param fecha Fecha en la que se desean consultar los viajes
     * @return Lista con los viajes ordenados 
     */
    IDoubleLinkedList<VOTrip> A3(int n, LocalDate fecha);
    
    /**
     * Buscar	bicicletas	para	mantenimiento
     * @param limiteInferior indica el tiempo minimo total recorrido que debe tener una bicicleta
     * @param limiteSuperior indica el tiempo m�ximo total recorrido que debe tener una bicicleta
     * @return arbol con las bicicletas
     */
    RedBlackBST<Integer, VOBike> B1(int limiteInferior, int limiteSuperior);


    /**
     * B�squeda	de	viajes	por	estaciones	de	salida	y	llegada	en	un	rango	de	tiempo.
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @param limiteInferiorTiempo tiempo m�nimo (en segundos) de lo que debe durar el recorrido para ser considerado
     * @param limiteSuperiorTiempo tiempo m�ximo (en segundos) de lo que debe durar el recorrido para ser considerado
     * @return Lista con los viajes ordenados
     */
    IDoubleLinkedList<VOTrip> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo);

    /**
     * Mostrar los viajes (ordenados cronologicamente)realizados por una bicicleta con el identificador dado que tengan
     * una duracion menor al valor de tiempo maximo dado y que hayan sido realizados por una persona del genero dado
     * @param bikeId El identificador de la bicicleta
     * @param tiempoMaximo La duracion maxima de los viajes
     * @param genero El genero de la persona que realizo los viajes
     * @return Lista con los viajes ordenados
     */
	int[] B3(String estacionDeInicio, String estacionDeLlegada);

	ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna);

	double[] C2(int lA, int lO);

	int darSector(double latitud, double longitud);

	IDoubleLinkedList<VOStation> C3(double latitud, double longitud);

	IDoubleLinkedList<VORoute> C4(double latitud, double longitud);

	IDoubleLinkedList<VORoute> C5(double latitudI, double longitudI, double latitudF, double longitudF);
}
