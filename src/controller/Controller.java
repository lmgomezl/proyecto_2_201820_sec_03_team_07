package controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

import model.data_structures.IDoubleLinkedList;
import model.data_structures.IHashDoubleLinkedList;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VORoute;
import model.vo.VOStation;
import model.vo.VOTrip;

public class Controller {
	private static DivvyTripsManager manager = new DivvyTripsManager();

	public static void cargarDatos(){
		manager.cargar();
	}

	public static Queue<VOTrip> A1(LocalDate fechaTerminacion,int n) {
		return manager.A1(fechaTerminacion, n);
	}

	public static IDoubleLinkedList<VOTrip> A2(int n){
		return manager.A2(n);
	}

	public static IDoubleLinkedList<VOTrip> A3(int n, LocalDate fecha) {
		return manager.A3(n, fecha);
	}

	public static RedBlackBST<Integer, VOBike> B1(int limiteInferior, int limiteSuperior) {
		return manager.B1(limiteInferior, limiteSuperior);
	}

	public static IDoubleLinkedList<VOTrip> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		return manager.B2(fechaInicial, fechaFinal, limiteInferiorTiempo, limiteSuperiorTiempo);
	}

	public static int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		return manager.B3(estacionDeInicio, estacionDeLlegada);
	}

	public static ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		return manager.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);
	}

	public static double[] C2(int LA, int LO) {
		return manager.C2(LA, LO);
	}

	public class ResultadoCampanna{
		public int costoTotal;
		public IDoubleLinkedList<VOStation> estaciones;
	}

	public static int darSector(double latitud, double longitud) {
		return manager.darSector(latitud, longitud);
	}

	public static IDoubleLinkedList<VOStation> C3(double latitud, double longitud) {
		return manager.C3(latitud, longitud);
	}

	public static IDoubleLinkedList<VORoute> C4(double latitud, double longitud) {
		return manager.C4(latitud, longitud);
	}

	public static IDoubleLinkedList<VORoute> C5(double latitudI, double longitudI, double latitudF, double longitudF) {
		return manager.C5(latitudI, longitudI, latitudF, longitudF);
	}

	public static int tripsSize()
	{
		return manager.getTripsSize();
	}

	public static int stationsSize()
	{
		return manager.getStationsSize();
	}
	
	public static int routesSize()
	{
		return manager.getRoutesSize();
	}

}

