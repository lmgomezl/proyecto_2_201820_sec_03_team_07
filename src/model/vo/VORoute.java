/**
 * 
 */
package model.vo;

/**
 * @author Luis Miguel Gomez Londono
 * Clase para representar las rutas de las bicicletas
 *
 */
public class VORoute implements Comparable<VORoute> {

	private String bikeRouteType;

	private String geom;

	private String refStreet;

	private String calleExtremo1;

	private String calleExtremo2;

	private double distancia;

	private double [] latitudes;

	private double [] longitudes;

	private double maxLat;

	private double maxLong;

	private double minLat;

	private double minLong;

	/**
	 * @param tipoCicloRuta
	 * @param ruta
	 * @param nombreCalleReferencia
	 * @param calleExtremo1
	 * @param calleExtremo2
	 * @param longitud
	 */
	public VORoute(String tipoCicloRuta, String ruta, String nombreCalleReferencia, String calleExtremo1,
			String calleExtremo2, double longitud) {
		this.bikeRouteType = tipoCicloRuta;
		this.geom = ruta;
		this.refStreet = nombreCalleReferencia;
		this.calleExtremo1 = calleExtremo1;
		this.calleExtremo2 = calleExtremo2;
		this.distancia = longitud;
		calcularLatitudesLongitudes();
		calcularMaxYMin();
	}

	public String getTipoCicloRuta() {
		return bikeRouteType;
	}

	public String getRuta() {
		return geom;
	}

	public String getNombreCalleReferencia() {
		return refStreet;
	}

	public String getCalleExtremo1() {
		return calleExtremo1;
	}

	public String getCalleExtremo2() {
		return calleExtremo2;
	}

	public double getLongitud() {
		return distancia;
	}

	public void calcularLatitudesLongitudes() {
		//cadena de parejas
		String completo = geom.substring(12, geom.length()-2);
		String [] parejas = completo.split(", ");
		latitudes = new double[parejas.length];
		longitudes = new double[parejas.length];
		for(int i = 0; i < parejas.length; i++)
		{
			String [] pareja = parejas[i].split(" ");
			System.out.println("pareja longitud latitud: "+pareja[0] + "      "+ pareja[1]);
			latitudes[i] = Double.parseDouble(pareja[0]);
			longitudes[i] = Double.parseDouble(pareja[1]);
		}

	}

	public void calcularMaxYMin()
	{
		double longMax = 0.0;
		double longMin = 50.0;
		double latMax = -88.0;
		double latMin = 0.0;

		for(int i = 0; i < latitudes.length; i++)
		{
			if(latitudes[i] >= latMax)
				latMax = latitudes[i];
			if(latitudes[i] <= latMin)
				latMin = latitudes[i];
			if(longitudes[i] >= longMax)
				longMax = longitudes[i];
			if(longitudes[i] <= longMin)
				longMin = longitudes[i];
		}
		maxLong = longMax;
		maxLat = latMax;
		minLong = longMin;
		minLat = latMin;	
	}

	public double getMaxLat()
	{
		return maxLat;
	}

	public double getMaxLong() 
	{
		return maxLong;
	}

	public double getMinLat()
	{
		return minLat;
	}

	public double getMinLong() 
	{
		return minLong;
	}
	
	public double[] darLongitudes()
	{
		return longitudes;
	}
	
	public double[] darLatitudes()
	{
		return latitudes;
	}

	@Override
	public int compareTo(VORoute o) {
		// TODO Auto-generated method stub
		return 0;
	}




}
