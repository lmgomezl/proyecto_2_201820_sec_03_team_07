package model.vo;

import java.util.ArrayList;

public class VOSector {

	private double maxLatitude;

	private double maxLongitude;

	private double minLatitude;

	private double minLongitude;
	
	public ArrayList<VORoute> cicloRutas;
	
	public ArrayList<VOStation> estaciones;
	
	public VOSector(double maxLat, double minLat, double maxLong, double minLong)
	{
		setMaxLatitude(maxLat);
		setMinLatitude(minLat);
		setMaxLongitude(maxLong);
		setMinLongitude(minLong);
		cicloRutas = new ArrayList<VORoute>();
		estaciones = new ArrayList<VOStation>();
	}

	public double getMaxLatitude() {
		return maxLatitude;
	}

	public void setMaxLatitude(double maxLatitude) {
		this.maxLatitude = maxLatitude;
	}

	public double getMaxLongitude() {
		return maxLongitude;
	}

	public void setMaxLongitude(double maxLongitude) {
		this.maxLongitude = maxLongitude;
	}

	public double getMinLatitude() {
		return minLatitude;
	}

	public void setMinLatitude(double minLatitude) {
		this.minLatitude = minLatitude;
	}

	public double getMinLongitude() {
		return minLongitude;
	}

	public void setMinLongitude(double minLongitude) {
		this.minLongitude = minLongitude;
	}
	
	public void agregarCicloRuta(VORoute cicloRuta)
	{
		cicloRutas.add(cicloRuta);
	}
	
	public void agregarEstacion(VOStation estacion)
	{
		estaciones.add(estacion);
	}
	
	
}
