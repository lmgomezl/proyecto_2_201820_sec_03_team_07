package model.vo;

public class VOBike{

	private int bikeId;
	private int totalTrips;
	private double totalDistance;
	private int totalDuration;

	public VOBike(int bikeId, double totalDistance, int ptotalDuration) {
		this.bikeId = bikeId;
		this.totalTrips = 1;
		this.totalDistance = totalDistance;
		this.totalDuration = ptotalDuration;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTotalTrips() {
		return totalTrips;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public int getTotalDuration() {
		return totalDuration;
	}
	
	public void sumarTrip()
	{
		totalTrips++;
	}
	
	public void sumarDistancia(double distancia)
	{
		totalDistance += distancia;
	}
	
	public void sumarDuracion(int duracion)
	{
		totalDuration += duracion;
	}
	
}
