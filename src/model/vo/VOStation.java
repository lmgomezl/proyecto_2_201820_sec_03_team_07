package model.vo;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Representacion del objeto de una estacion. 
 * @author Luis Miguel Gomez Londono.
 *
 */
public class VOStation {

	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int dpcapacity;
	
	private LocalDateTime online_date;
	
	private int points;

	/**
	 * @param id
	 * @param name
	 * @param city
	 * @param latitude
	 * @param longitude
	 * @param dpcapacity
	 * @param online_date
	 */
	public VOStation(int id, String name, String city, double latitude, double longitude, int dpcapacity,
			String online_date) {
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpcapacity = dpcapacity;
		this.online_date = setDate(online_date);
		points = 0;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return the dpcapacity
	 */
	public int getDpcapacity() {
		return dpcapacity;
	}
	
	public int getPoints()
	{
		return points;
	}
	
	public void setPoints()
	{
		points ++;
	}

	/**
	 * @return la fecha en forma Date de la que se recibio en forma de cadena en la entrada.
	 */
	private static LocalDateTime setDate(String pFecha)
	{
		String [] completo = pFecha.split(" ");
		String fecha = completo[0];
		String hora = completo[1];
		
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;
		try{
			segundos = Integer.parseInt(datosHora[2]);
		}
		catch(Exception e)
		{
			return LocalDateTime.of(agno, mes, dia, horas, minutos, 0); 
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
	
	/**
	 * @return la fecha de inicio de operaciones de la estacion. 
	 */
	public LocalDateTime getOnline_date()
	{
		return online_date;
	}
	
}