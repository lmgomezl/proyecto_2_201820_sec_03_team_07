package model.vo;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Representation of a Trip object
 * @author Luis Miguel Gomez Londono
 */
public class VOTrip implements Comparable<VOTrip>{

	private int trip_id;

	private LocalDateTime start_time;

	private LocalDateTime end_time;

	private int bikeid;

	private int tripduration;

	private int from_station_id;

	private String from_station_name;

	private int to_station_id;

	private String to_station_name;

	private String usertype;

	private String gender;

	private int birthyear;
	
	private double totalDistance;
	

	/**
	 * @param trip_id
	 * @param start_time
	 * @param end_time
	 * @param bikeid
	 * @param tripduration
	 * @param from_station_id
	 * @param from_station_name
	 * @param to_station_id
	 * @param to_station_name
	 * @param usertype
	 * @param birthyear
	 */
	public VOTrip(int trip_id, LocalDateTime start_time, LocalDateTime end_time, int bikeid, int tripduration, int from_station_id,
			String from_station_name, int to_station_id, String to_station_name, String gender , String usertype, int birthyear, double totalDistance) {
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeid = bikeid;
		this.tripduration = tripduration;
		this.from_station_id = from_station_id;
		this.from_station_name = from_station_name;
		this.to_station_id = to_station_id;
		this.to_station_name = to_station_name;
		this.usertype = usertype;
		this.gender = gender;
		this.birthyear = birthyear;
		this.totalDistance = totalDistance;
	}

	/**
	 * @return the trip_id
	 */
	public int getTrip_id() {
		return trip_id;
	}

	/**
	 * @return the start_time
	 */
	public LocalDateTime getStart_time() {
		return start_time;
	}

	/**
	 * @return the end_time
	 */
	public LocalDateTime getEnd_time() {
		return end_time;
	}

	/**
	 * @return the bikeid
	 */
	public int getBikeid() {
		return bikeid;
	}

	/**
	 * @return the tripduration
	 */
	public int getTripduration() {
		return tripduration;
	}

	/**
	 * @return the from_station_id
	 */
	public int getFrom_station_id() {
		return from_station_id;
	}

	/**
	 * @return the from_station_name
	 */
	public String getFrom_station_name() {
		return from_station_name;
	}

	/**
	 * @return the to_station_id
	 */
	public int getTo_station_id() {
		return to_station_id;
	}

	/**
	 * @return the to_station_name
	 */
	public String getTo_station_name() {
		return to_station_name;
	}

	/**
	 * @return the usertype
	 */
	public String getUsertype() {
		return usertype;
	}

	/**
	 * @return the birthyear
	 */
	public int getBirthyear() {
		return birthyear;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	
	/**
	 * @return the totalDistance
	 */
	public double getTotalDistance()
	{
		return totalDistance;
	}
	
	public Date convertirStringAFecha(String str)
	{
		Calendar calendario = Calendar.getInstance();
		String[] fecha = str.split("/");
		int dia = Integer.parseInt(fecha[2]);
		int mes = Integer.parseInt(fecha[1]);
		int anio = Integer.parseInt(fecha[0]);
		int hora = Integer.parseInt(fecha[3]);
		int min = Integer.parseInt(fecha[4]);
		int seg = Integer.parseInt(fecha[5]);
		calendario.set(anio,mes,dia,hora,min,seg);
		return calendario.getTime();
	}

	@Override
	public int compareTo(VOTrip o) {
		if(this.tripduration > o.getTripduration())
			return 1;
		else if(this.tripduration < o.getTripduration())
			return -1;
		else
			return 0;
	}

}
