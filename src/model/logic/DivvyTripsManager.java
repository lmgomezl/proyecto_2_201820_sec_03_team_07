package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import com.google.gson.stream.*;
import api.IDivvyTripsManager;
import controller.Controller.ResultadoCampanna;
import model.vo.VOBike;
import model.vo.VORoute;
import model.vo.VOSector;
import model.vo.VOStation;
import model.vo.VOTrip;
import proyecto_2_201820_sec_03_team_07.ArbolBinarioRojoNegroTest;
import model.data_structures.DoubleLinkedList;
import model.data_structures.HashDoubleLinkedList;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.IHashDoubleLinkedList;
import model.data_structures.LinearProbingHash;
import model.data_structures.LinkedListOrdered;
import model.data_structures.MaxHeapCP;
import model.data_structures.NodoDobleHash;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHash;

/**
 * Manager Proyecto 2 201820
 * @author luisgomez
 *
 */
public class DivvyTripsManager implements IDivvyTripsManager {

	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "data"+ File.separator + "Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "data"+ File.separator + "Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "data"+ File.separator + "Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "data"+ File.separator + "Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "data"+ File.separator + "Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "data"+ File.separator + "Divvy_Stations_2017_Q3Q4.csv";

	public static final String BIKE_ROUTES = "data"+ File.separator + "CDOT_Bike_Routes_2014_1216-transformed.json";

	/**
	 * Atributo que representa el arbol de trips
	 */
	private Queue<VOTrip> trips = new Queue<>();

	private RedBlackBST<Integer, VOBike> bicicletas= new RedBlackBST<>();

	private RedBlackBST<Integer, VOStation> stations = new RedBlackBST<>();

	private Queue<VORoute> routes = new Queue<VORoute>();

	private double maxLatitude;

	private double maxLongitude;

	private double minLatitude;

	private double minLongitude;

	private int maxDurationTrip = -1;
	
	int LA;
	
	int LO;
	
	SeparateChainingHash<Integer, VOSector> sectorizacion;
	
	public void cargar()
	{
		loadTrips(TRIPS_Q1);
		loadTrips(TRIPS_Q2);
		loadTrips(TRIPS_Q3);
		loadTrips(TRIPS_Q4);
		loadStations(STATIONS_Q1_Q2);
		loadStations(STATIONS_Q3_Q4);
		loadBikeRoutesJSON(BIKE_ROUTES);

	}

	/**
	 * Metodo que carga las ciclorutas en formato JSON con la herramienta GSON de Google
	 * @param jsonFile - el archivo json que se va a cargar.
	 */
	@Override
	public void loadBikeRoutesJSON(String jsonFile) {
		//Deserializacion de GSON JSON-> Objeto de Java, habia que renombrar 
		try {
			JsonReader reader = new JsonReader(new FileReader(jsonFile));
			reader.beginArray();
			while(reader.hasNext())
			{
				routes.enqueue(leerRutaJSON(reader));
			}
			reader.endArray();
			reader.close();
			calcularMaxMinLatLong();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Continuacion del metodo de loadBikeRoutesJSON, este navega entre cada objeto y crea un VORoute por cada
	 * Invocacion de este metodo.
	 * @param pReader el reader que se usa en el mismo bikeRoutes JSON
	 * @return el VORoute que se adquiere del JSON
	 * @throws IOException
	 */
	public VORoute leerRutaJSON(JsonReader pReader) throws IOException
	{
		String bikeRoute = "N/A";
		String geom = "N/A";
		String refStreet = "N/A";
		String fstreet = "N/A";
		String tstreet = "N/A";
		double routeLength = 0.0;

		pReader.beginObject();
		while(pReader.hasNext())
		{
			String n = pReader.nextName();
			JsonToken check = pReader.peek();
			if(n.equals("BIKEROUTE") && check != JsonToken.NULL)
				bikeRoute = pReader.nextString();
			else if(n.equals("the_geom") && check != JsonToken.NULL)
				geom = pReader.nextString();
			else if(n.equals("STREET") && check != JsonToken.NULL)
				refStreet = pReader.nextString();
			else if(n.equals("F_STREET") && check != JsonToken.NULL)
				fstreet = pReader.nextString();
			else if(n.equals("T_STREET") && check != JsonToken.NULL)
				tstreet = pReader.nextString();
			else if(n.equals("Shape_Leng"))
				routeLength = pReader.nextDouble();
			else
				pReader.skipValue();
		}
		pReader.endObject();
		//System.out.println(bikeRoute + "  " + geom + "  " + refStreet + "  " + fstreet + "  " + tstreet + "  " + routeLength );
		return new VORoute(bikeRoute, geom, refStreet, fstreet, tstreet, routeLength);		
	}

	/**
	 * metodo que carga todas las trips de un archivo .csv compatible con el formato.
	 */
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(tripsFile)));
			in.readLine();
			String l =  in.readLine();
			while(l != null)
			{

				int trip_id;
				String start_time;
				String end_time;
				int bikeid;
				int tripduration;
				int from_station_id;
				String from_station_name;
				int to_station_id;
				String to_station_name;
				String usertype;
				String gender;
				int birthYear;

				if(l.startsWith("\""))
				{
					String [] partes = l.split(",");
					trip_id = Integer.parseInt(partes[0].substring(1, partes[0].length()-1));
					start_time = partes[1].substring(1, partes[1].length()-1);
					end_time = partes[2].substring(1, partes[2].length()-1);
					bikeid = Integer.parseInt(partes[3].substring(1, partes[3].length()-1));
					tripduration = Integer.parseInt(partes[4].substring(1, partes[4].length()-1));
					if(tripduration>maxDurationTrip)
					{
						maxDurationTrip = tripduration;
					}
					from_station_id = Integer.parseInt(partes[5].substring(1, partes[5].length()-1));
					from_station_name = partes[6].substring(1, partes[6].length()-1);
					to_station_id = Integer.parseInt(partes[7].substring(1, partes[7].length()-1));
					to_station_name = partes[8].substring(1, partes[8].length()-1);
					usertype = partes[9].substring(1, partes[9].length()-1);
					gender = "N/A";
					birthYear = 0;
					if(partes[10] == ("\"" + "\""))
					{
						gender = partes[10].substring(1, partes[10].length()-1);
					}
					if(partes[11] == ("\"" + "\""))
					{
						birthYear = Integer.parseInt(partes[11].substring(1, partes[11].length()-1));				
					}
				}
				else
				{
					String [] partes = l.split(",");
					trip_id = Integer.parseInt(partes[0]);
					start_time = partes[1];
					end_time = partes[2];
					bikeid = Integer.parseInt(partes[3]);
					tripduration = Integer.parseInt(partes[4]);
					if(tripduration>maxDurationTrip)
					{
						maxDurationTrip = tripduration;
					}
					from_station_id = Integer.parseInt(partes[5]);
					from_station_name = partes[6];
					to_station_id = Integer.parseInt(partes[7]);
					to_station_name = partes[8];
					usertype = partes[9];
					gender = "N/A";
					birthYear = 0;
					if(partes.length == 11)
					{
						gender = partes[10];
					}
					if(partes.length == 12)
					{
						gender = partes[10];
						birthYear = Integer.parseInt(partes[11]);				
					}
				}

				double distancia = calcularDistancia(from_station_id, to_station_id);
				VOTrip nuevo = new VOTrip(trip_id, setDate(start_time), setDate(end_time), bikeid, tripduration, from_station_id, from_station_name, to_station_id, to_station_name, gender, usertype, birthYear, distancia);


				VOBike bikeConId = bicicletas.get(bikeid);
				if(bikeConId != null)
				{
					bicicletas.get(bikeid).sumarTrip();
					bicicletas.get(bikeid).sumarDistancia(distancia);
					bicicletas.get(bikeid).sumarDuracion(tripduration);
				}
				else
				{
					VOBike nueva = new VOBike(bikeid, distancia, tripduration);
					bicicletas.put(bikeid, nueva);
				}

				trips.enqueue(nuevo);
				l = in.readLine();
			}
			//System.out.println("Tamanio del arbol: "+arbolTripsA1.size());
			in.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @return la fecha en forma Date de la que se recibio en forma de cadena en la entrada.
	 */
	private static LocalDateTime setDate(String pFecha)
	{
		String [] completo = pFecha.split(" ");
		String fecha = completo[0];
		String hora = completo[1];

		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;
		try{

			segundos = Integer.parseInt(datosHora[2]);
		}
		catch(Exception e)
		{
			return LocalDateTime.of(agno, mes, dia, horas, minutos, 0);
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}

	/**
	 * metodo que carga todas las estaciones de un archivo .csv compatible con el formato.
	 */
	public void loadStations (String stationsFile) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(stationsFile)));
			// nos saltamos la primera linea.
			in.readLine();
			String l = in.readLine();
			while(l != null)
			{

				String [] partes = l.split(",");
				int id;
				String name;
				String city;
				double latitude;
				double longitude;
				int dpcapacity;
				String online_date;
				if(stationsFile.substring(5).equals("Divvy_Stations_2017_Q3Q4.csv")){
					id = Integer.parseInt(partes[0]);
					name = partes[1];
					city = partes[2];
					latitude = Double.parseDouble(partes[3]);
					longitude = Double.parseDouble(partes[4]);
					dpcapacity = Integer.parseInt(partes[5]);
					online_date = partes[6];
				}
				else if(l.startsWith("\""))
				{
					id = Integer.parseInt(partes[0].substring(1, partes[0].length()-1));
					name = partes[1].substring(1, partes[1].length()-1);;
					city = partes[2].substring(1, partes[2].length()-1);;
					latitude = Double.parseDouble(partes[3].substring(1, partes[3].length()-1));
					longitude = Double.parseDouble(partes[4].substring(1, partes[4].length()-1));
					dpcapacity = Integer.parseInt(partes[5].substring(1, partes[5].length()-1));
					online_date = partes[6].substring(1, partes[6].length()-1);
				}
				else
				{
					id = Integer.parseInt(partes[0]);
					name = partes[1];
					city = partes[2];
					latitude = Double.parseDouble(partes[3]);
					longitude = Double.parseDouble(partes[4]);
					dpcapacity = Integer.parseInt(partes[5]);
					online_date = partes[6];
				}
				VOStation nuevo = new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date);
				stations.put(id, nuevo);

				l = in.readLine();					

			}

			in.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	/**
	 * Calcula la disntancia entre dos estaciones.
	 * @param id1 id estacion 1
	 * @param id2 id estacion 2
	 * @return la distancia entre las dos
	 */
	public double calcularDistancia(int id1, int id2)
	{

		double latitudS1 = 0.0;
		double longitudeS1 = 0.0;
		double latitudS2 = 0.0;
		double longitudeS2 = 0.0;
		Queue<VOStation> estacionesIt = stations.values();

		for(VOStation actual: estacionesIt)
		{
			if(actual.getId() == id1)
			{
				latitudS1 = actual.getLatitude();
				longitudeS1 = actual.getLongitude();
			}

			if(actual.getId() == id2)
			{
				latitudS2 = actual.getLatitude();
				longitudeS2 = actual.getLongitude();
			}
		}
		return Haversine.distance(latitudS1, longitudeS1, latitudS2, longitudeS2);

	}


	public int getTripsSize()
	{
		return trips.size();
	}

	public int getStationsSize()
	{
		return stations.size();
	}

	public int getRoutesSize()
	{
		return routes.size();
	}

	@Override
	public void calcularMaxMinLatLong() {

		//Asumiendo mayores y menores se pueden inferir estos estados inciales...
		//Como la latitud no va a ser menor que 87........ entonces ponemos algo muy minimo para que busque hacia arriba.
		double latMax = -88.0;
		//Como la latitud no va a ser mayor que 0........ entonces ponemos algo muy max para que busque hacia abajo.
		double latMin = 0.0;
		//Como la longitud no va a ser menor que 0........ entonces ponemos algo muy minimo para que busque hacia arriba.
		double longMax = 0.0;
		//Como la longitud no va a ser mayor que 50........ entonces ponemos algo muy max para que busque hacia abajo.
		double longMin = 50.0;

		for(VORoute actual: routes)
		{
			if(actual.getMaxLat() >= latMax)
				latMax = actual.getMaxLat();
			if(actual.getMinLat() <= latMin)
				latMin = actual.getMinLat();
			if(actual.getMaxLong() >= longMax)
				longMax = actual.getMaxLong();
			if(actual.getMinLong() <= longMin)
				longMin = actual.getMinLong();
		}

		maxLongitude = latMax;
		maxLatitude = longMax;
		minLongitude = latMin;
		minLatitude = longMin;
	}

	/**
	 * @return the maxLatitude
	 */
	public double getMaxLatitude() {
		return maxLatitude;
	}

	/**
	 * @return the maxLongitude
	 */
	public double getMaxLongitude() {
		return maxLongitude;
	}

	/**
	 * @return the minLatitude
	 */
	public double getMinLatitude() {
		return minLatitude;
	}

	/**
	 * @return the minLongitude
	 */
	public double getMinLongitude() {
		return minLongitude;
	}

	@Override
	public ArrayList<VORoute> buscarCiclaRutasSeparateChaining(double latitude, double longitude) throws Exception {
		// TODO Auto-generated method stub
		//x es el rango en el eje x (latitud) que cubre un sector
		double x = (maxLongitude-minLongitude)/10;
		//y es el rango en el eje y (longitud) que cubre un sector

		double y = ((minLatitude)-(maxLatitude))/10;

		// 1))))))))))) PRIMERO INGRESO LAS CICLORUTAS AL SEPARATEchianing
		SeparateChainingHash<Integer, VOSector> hashSeparate = new SeparateChainingHash<Integer,VOSector>(100);
		for(VORoute ruta : routes)
		{
			double latitudeInicial = ruta.darLatitudes()[0];

			double latitudeFinal = ruta.darLatitudes()[ruta.darLatitudes().length-1];
			double longitudInicial = ruta.darLongitudes()[0];

			double longitudFinal = ruta.darLongitudes()[ruta.darLongitudes().length-1];

			//Calculo en que sector queda la latitud y longitud inicial
			int sector1y = (int)Math.floor((latitudeInicial-maxLatitude)/y)+1; 
			int sector1x = (int)Math.floor((longitudInicial-minLongitude)/x)+1;
			int sector1 = sector1x+(10*(sector1y-1));


			//Calculo en que sector queda la longitud y latitud final
			int sector2y =  (int)Math.floor((latitudeFinal-maxLatitude)/y)+1;
			int sector2x = (int)Math.floor((longitudFinal-minLongitude)/x)+1;
			int sector2 = sector2x+(10*(sector2y-1));

			if(sector1==sector2)
			{

				VOSector este = hashSeparate.get(sector1);
				if(este==null)
				{
					VOSector nuevo = new VOSector(((sector1x+1)*x)+minLatitude, sector1x*x+minLatitude, ((sector1y+y)*y)+minLongitude, ((sector1y*y)+minLongitude));
					hashSeparate.put(sector1, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					este.agregarCicloRuta(ruta);
				}
			}
			else
			{

				VOSector sectoR1 = hashSeparate.get(sector1);
				VOSector sectoR2 = hashSeparate.get(sector2);
				if(sectoR1==null)
				{
					VOSector nuevo = new VOSector(((sector1x+1)*x)+minLatitude, sector1x*x+minLatitude, ((sector1y+y)*y)+minLongitude, ((sector1y*y)+minLongitude));
					hashSeparate.put(sector1, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					sectoR1.agregarCicloRuta(ruta);
				}

				if(sectoR2==null)
				{
					VOSector nuevo = new VOSector(((sector2x+1)*x)+minLatitude, sector2x*x+minLatitude, ((sector2y+y)*y)+minLongitude, ((sector2y*y)+minLongitude));
					hashSeparate.put(sector2, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					sectoR2.agregarCicloRuta(ruta);
				}
			}
		}
		// 2))))))))))))))) BUSCO LAS RUTAS QUE EST�N EN EL SECTOR
		int secy = (int)Math.floor((latitude-maxLatitude)/y)+1; 
		int secx = (int)Math.floor((longitude-minLongitude)/x)+1;
		int numSector = secx+(10*(secy-1));
		System.out.println("Se encuentra en el sector: " + numSector);

		if(hashSeparate.get(numSector)==null)
		{
			return null;
		}
		else
			return hashSeparate.get(numSector).cicloRutas;

	}

	@Override
	public ArrayList<VORoute> buscarCiclaRutasLinearProbing(double longitude, double latitude) {
		// TODO Auto-generated method stub
		double x = (maxLongitude-minLongitude)/10;
		//y es el rango en el eje y (longitud) que cubre un sector

		double y = ((minLatitude)-(maxLatitude))/10;

		// 1))))))))))) PRIMERO INGRESO LAS CICLORUTAS AL SEPARATEchianing
		LinearProbingHash<Integer, VOSector> hashSeparate = new LinearProbingHash<Integer,VOSector>(100);
		for(VORoute ruta : routes)
		{
			double latitudeInicial = ruta.darLatitudes()[0];

			double latitudeFinal = ruta.darLatitudes()[ruta.darLatitudes().length-1];
			double longitudInicial = ruta.darLongitudes()[0];

			double longitudFinal = ruta.darLongitudes()[ruta.darLongitudes().length-1];

			//Calculo en que sector queda la latitud y longitud inicial
			int sector1y = (int)Math.floor((latitudeInicial-maxLatitude)/y)+1; 
			int sector1x = (int)Math.floor((longitudInicial-minLongitude)/x)+1;
			int sector1 = sector1x+(10*(sector1y-1));


			//Calculo en que sector queda la longitud y latitud final
			int sector2y =  (int)Math.floor((latitudeFinal-maxLatitude)/y)+1;
			int sector2x = (int)Math.floor((longitudFinal-minLongitude)/x)+1;
			int sector2 = sector2x+(10*(sector2y-1));

			if(sector1==sector2)
			{
				VOSector este = hashSeparate.get(sector1);
				if(este==null)
				{
					VOSector nuevo = new VOSector(((sector1x+1)*x)+minLatitude, sector1x*x+minLatitude, ((sector1y+y)*y)+minLongitude, ((sector1y*y)+minLongitude));
					hashSeparate.put(sector1, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					este.agregarCicloRuta(ruta);
				}
			}
			else
			{

				VOSector sectoR1 = hashSeparate.get(sector1);
				VOSector sectoR2 = hashSeparate.get(sector2);
				if(sectoR1==null)
				{
					VOSector nuevo = new VOSector(((sector1x+1)*x)+minLatitude, sector1x*x+minLatitude, ((sector1y+y)*y)+minLongitude, ((sector1y*y)+minLongitude));
					hashSeparate.put(sector1, nuevo);
					VOSector nuevoS = hashSeparate.get(sector1);

					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					sectoR1.agregarCicloRuta(ruta);
				}

				if(sectoR2==null)
				{
					VOSector nuevo = new VOSector(((sector2x+1)*x)+minLatitude, sector2x*x+minLatitude, ((sector2y+y)*y)+minLongitude, ((sector2y*y)+minLongitude));
					hashSeparate.put(sector2, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					sectoR2.agregarCicloRuta(ruta);
				}
			}
		}
		// 2))))))))))))))) BUSCO LAS RUTAS QUE EST�N EN EL SECTOR
		int secy = (int)Math.floor((latitude-maxLatitude)/y)+1; 
		int secx = (int)Math.floor((longitude-minLongitude)/x)+1;
		int numSector = secx+(10*(secy-1));
		System.out.println("Se encuentra en el sector: " + numSector);
		if(hashSeparate.get(numSector)==null)
		{
			return null;
		}
		else
			return hashSeparate.get(numSector).cicloRutas;
	}

	//Cambiar parametro a LocalDate
	@Override
	public Queue<VOTrip> A1(LocalDate fechaFinal,int pCapcidadMinima) {
		// TODO Auto-generated method stub
		Queue<VOTrip> tripes = trips;
		Queue<VOStation> estacionesQ = stations.values();
		RedBlackBST<Date, VOTrip> arbolTrips = new RedBlackBST<>();
		LinearProbingHash<Integer, VOStation> estacionesH = new LinearProbingHash<>(100);
		LocalDateTime fechaIniDia = fechaFinal.atTime(0, 0);
		LocalDateTime fechaFiniDia=fechaFinal.atTime(23,59);
		for(VOStation actual : estacionesQ)
		{
			estacionesH.put(actual.getId(), actual);

		}
		for(VOTrip actual : tripes )
		{
			int comp1 = fechaIniDia.compareTo(actual.getStart_time());
			int comp2 = fechaFiniDia.compareTo(actual.getEnd_time());
			if(comp1<=0 && comp2>=0 && estacionesH.get(actual.getTo_station_id()).getDpcapacity()>=pCapcidadMinima)
			{
				int id = actual.getTrip_id();
				LocalDateTime fechaInicio = actual.getStart_time();
				LocalDateTime fechaFin = actual.getEnd_time();
				int duration = actual.getTripduration();
				int idBike = actual.getBikeid();
				int stationIdIni = actual.getFrom_station_id();
				String stationIniName = actual.getFrom_station_name();
				int stationIdFin = actual.getTo_station_id();
				String stationFinName = actual.getTo_station_name();
				String userT = actual.getUsertype();
				String gender = actual.getGender();
				int birth = actual.getBirthyear();
				double distance = actual.getTotalDistance();

				VOTrip nuevo = new VOTrip(id, fechaInicio, fechaFin, idBike, duration, stationIdIni, stationIniName, stationIdFin, stationFinName, gender, userT, birth, distance);
				Date fech = new Date();
				Date out = Date.from(fechaFin.atZone(ZoneId.systemDefault()).toInstant());
				arbolTrips.put(out, nuevo);
			}
		}
		return arbolTrips.values();
	}

	//Cambiar Retorno a IDoubleLinkedList
	@Override
	public IDoubleLinkedList<VOTrip> A2(int pDuracion) {
		// TODO Auto-generated method stub
		Queue<VOTrip> tripes = trips;
		int tam = Math.floorDiv(maxDurationTrip,60);
		if(tam%2==1)
		{
			tam++;
		}
		int re = Math.floorDiv(pDuracion, 60)/2;
		if(re%2!=0)
			re++;
		//System.out.println(tam + " num total");
		SeparateChainingHash<Integer, VOTrip> tabla = new SeparateChainingHash<>(tam/2);
		VOTrip ultimo = tripes.dequeue();
		System.out.println(trips.size()+"TAMANIO TRIPS");
		while(ultimo!=null)
		{
			int key = Math.floorDiv(ultimo.getTripduration(), 60);
			int key1 = key/2;
			if(key1%2!=0)
				key1++;
			tabla.put(key, ultimo);
			ultimo = tripes.dequeue();
		}

		IHashDoubleLinkedList<Integer, VOTrip> lista = tabla.getlist(re);
		IDoubleLinkedList<VOTrip> listaValores = new DoubleLinkedList<>();
		for(Integer actual: lista)
		{
			listaValores.add(tabla.get(actual.intValue()));
		}

		return listaValores;
	}

	@Override
	public IDoubleLinkedList<VOTrip> A3(int n, LocalDate fecha) {
		// TODO Auto-generated method stub
		MaxHeapCP<VOTrip> viajes = new MaxHeapCP<>(trips.size());
		Queue<VOTrip> tripsQ = trips;
		LocalDateTime fechaIniDia = fecha.atTime(0, 0);
		LocalDateTime fechaFiniDia=fecha.atTime(23,59);
		for(VOTrip actual : tripsQ)
		{
			int comp1 = fechaIniDia.compareTo(actual.getStart_time());
			int comp2 = fechaFiniDia.compareTo(actual.getEnd_time());
			if(comp1<=0 && comp2>=0)
			{
				viajes.agregar(actual);
			}
		}
		IDoubleLinkedList<VOTrip> nViajes = new DoubleLinkedList<>();
		for(int i = 0; i < n; i++)
		{
			if(!viajes.esVacia())
			{
				try {
					nViajes.add(viajes.max());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		}

		return nViajes;
	}

	@Override
	public RedBlackBST<Integer, VOBike> B1(int limiteInferior, int limiteSuperior) {
		RedBlackBST<Integer, VOBike> lista = new RedBlackBST<>();
		Queue<VOBike> bikeValues = bicicletas.values();

		for(VOBike actual: bikeValues)
		{
			if(actual.getTotalDuration() >= limiteInferior &&
					actual.getTotalDuration() <= limiteSuperior)
			{
				lista.put(actual.getTotalDuration(), actual);
			}
		}
		return lista;
	}

	@Override
	public IDoubleLinkedList<VOTrip> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		LinearProbingHash<String, RedBlackBST<Integer, VOTrip>> tabla = new LinearProbingHash<>(10);
		Queue<VOTrip> viajes = trips;

		for(VOTrip actual: viajes)
		{
			String llave = actual.getFrom_station_name() + "-" + actual.getTo_station_name();
			RedBlackBST<Integer, VOTrip> arbActual = tabla.get(llave);
			if(arbActual != null)
			{
				if(fechaInicial.compareTo(actual.getStart_time()) <= 0 && 
						fechaFinal.compareTo(actual.getEnd_time()) >= 0 &&
						limiteInferiorTiempo <= actual.getTripduration() &&
						limiteSuperiorTiempo >= actual.getTripduration())
				{
					arbActual.put(actual.getTripduration(), actual);
				}
			}
			else
				tabla.put(llave, new RedBlackBST<Integer, VOTrip>());
		}

		IDoubleLinkedList<VOTrip> listaViajesFinal = new DoubleLinkedList<>();
		RedBlackBST<Integer, VOTrip> [] valores = tabla.getValues();
		for(RedBlackBST<Integer, VOTrip> actual: valores)
		{
			for(VOTrip trip: actual.values())
			{
				listaViajesFinal.add(trip);
			}
		}

		return listaViajesFinal;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		RedBlackBST<String, VOTrip> tabla = new RedBlackBST<>();
		Queue<VOTrip> viajes = trips;

		for(VOTrip actual: viajes)
		{
			if(actual.getFrom_station_name().equals(estacionDeInicio) &&
					actual.getTo_station_name().equals(estacionDeLlegada))
			{
				tabla.put(actual.getFrom_station_name(), actual);				
			}
		}

		SeparateChainingHash<Integer, VOTrip> horas = new SeparateChainingHash<>(24);
		Queue<VOTrip> tripsPorEIEF = tabla.values();

		for(VOTrip actual: tripsPorEIEF)
		{
			int horaInic = actual.getStart_time().getHour();
			horas.put(horaInic, actual);
		}

		IHashDoubleLinkedList<Integer, VOTrip>[] losViajes = horas.darLista();

		int horaMayor = 0;
		int cantidadViajes = 0;
		for(int i = 0; i < losViajes.length; i++)
		{
			IHashDoubleLinkedList<Integer, VOTrip> actual = losViajes[i];
			if(actual.getSize() > cantidadViajes)
			{
				horaMayor = i;
				cantidadViajes = actual.getSize();
			}			
		}

		return new int[] {horaMayor, cantidadViajes};
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {

		for(VOTrip actual: trips)
		{
			int inicS = actual.getFrom_station_id();
			int finalS = actual.getTo_station_id();
			String type = actual.getUsertype();

			if(type.equals("Subscriber"))
			{
				stations.get(inicS).setPoints();	
				stations.get(finalS).setPoints();				
			}
			stations.get(inicS).setPoints();	
			stations.get(finalS).setPoints();
		}

		RedBlackBST<Integer, VOStation> sPuntaje = new RedBlackBST<>();
		for(VOStation actual: stations.values())
		{
			sPuntaje.put(actual.getPoints(), actual);
		}



		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		//x es el rango en el eje x (latitud) que cubre un sector
		this.LA = LA;
		this.LO = LO;
		double x = (maxLongitude-minLongitude)/LO;
		//y es el rango en el eje y (longitud) que cubre un sector

		double y = ((minLatitude)-(maxLatitude))/LA;

		// 1))))))))))) PRIMERO INGRESO LAS CICLORUTAS AL SEPARATEchianing
		 sectorizacion = new SeparateChainingHash<Integer,VOSector>(LA*LO);
		for(VORoute ruta : routes)
		{
			double latitudeInicial = ruta.darLatitudes()[0];

			double latitudeFinal = ruta.darLatitudes()[ruta.darLatitudes().length-1];
			double longitudInicial = ruta.darLongitudes()[0];

			double longitudFinal = ruta.darLongitudes()[ruta.darLongitudes().length-1];

			//Calculo en que sector queda la latitud y longitud inicial
			int sector1y = (int)Math.floor((latitudeInicial-maxLatitude)/y)+1; 
			int sector1x = (int)Math.floor((longitudInicial-minLongitude)/x)+1;
			int sector1 = sector1x+(10*(sector1y-1));


			//Calculo en que sector queda la longitud y latitud final
			int sector2y =  (int)Math.floor((latitudeFinal-maxLatitude)/y)+1;
			int sector2x = (int)Math.floor((longitudFinal-minLongitude)/x)+1;
			int sector2 = sector2x+(10*(sector2y-1));

			if(sector1==sector2)
			{

				VOSector este = sectorizacion.get(sector1);
				if(este==null)
				{
					VOSector nuevo = new VOSector(((sector1x+1)*x)+minLatitude, sector1x*x+minLatitude, ((sector1y+y)*y)+minLongitude, ((sector1y*y)+minLongitude));
					sectorizacion.put(sector1, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					este.agregarCicloRuta(ruta);
				}
			}
			else
			{

				VOSector sectoR1 = sectorizacion.get(sector1);
				VOSector sectoR2 = sectorizacion.get(sector2);
				if(sectoR1==null)
				{
					VOSector nuevo = new VOSector(((sector1x+1)*x)+minLatitude, sector1x*x+minLatitude, ((sector1y+y)*y)+minLongitude, ((sector1y*y)+minLongitude));
					sectorizacion.put(sector1, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					sectoR1.agregarCicloRuta(ruta);
				}

				if(sectoR2==null)
				{
					VOSector nuevo = new VOSector(((sector2x+1)*x)+minLatitude, sector2x*x+minLatitude, ((sector2y+y)*y)+minLongitude, ((sector2y*y)+minLongitude));
					sectorizacion.put(sector2, nuevo);
					nuevo.agregarCicloRuta(ruta);
				}
				else
				{
					sectoR2.agregarCicloRuta(ruta);
				}
			}
		}
		return new double[] {maxLongitude,minLongitude,maxLatitude,minLatitude};
	}

	@Override
	public int darSector(double latitud, double longitud) {
		// TODO Auto-generated method stub
		int secy = (int)Math.floor((latitud-maxLatitude)/LA)+1; 
		int secx = (int)Math.floor((longitud-minLongitude)/LO)+1;
		int numSector = secx+(10*(secy-1));
		return numSector;
	}

	@Override
	public IDoubleLinkedList<VOStation> C3(double latitud, double longitud) {
		// TODO Auto-generated method stub
		Queue<VOStation> estacionesQ = stations.values();
		DoubleLinkedList<VOStation> respuesta = new DoubleLinkedList<>();
		for(VOStation actual : estacionesQ)
		{
			int num = darSector(actual.getLatitude(),actual.getLongitude());
			if(sectorizacion.get(num)!=null)
			sectorizacion.get(num).agregarEstacion(actual);
		}
		
		if(sectorizacion.get(darSector(latitud, longitud))!=null)
		{
			for(VOStation actual: sectorizacion.get(darSector(latitud, longitud)).estaciones)
			{
				respuesta.add(actual);
			}
		}
		return respuesta;
	}

	@Override
	public IDoubleLinkedList<VORoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		int num = darSector(latitud, longitud);
		DoubleLinkedList<VORoute> respuesta = new DoubleLinkedList<>();
		if(sectorizacion.get(num)!=null && !sectorizacion.get(num).cicloRutas.isEmpty())
		for(VORoute actual : sectorizacion.get(num).cicloRutas )
		{
			respuesta.add(actual);
		}
		return respuesta;
	}

	@Override
	public IDoubleLinkedList<VORoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub
		int pos1 = darSector(latitudI, longitudI);
		int pos2 = darSector(latitudF, longitudF);
		DoubleLinkedList<VORoute> respuesta = new DoubleLinkedList<>();
		boolean pos1B = false;
		boolean pos2B = false;
		if(sectorizacion.get(pos1)!=null && !sectorizacion.get(pos1).cicloRutas.isEmpty())
		{
			for(VORoute actual : sectorizacion.get(pos1).cicloRutas)
			{
				pos1B = false;
				pos2B = false;
				for(int e=0; e<actual.darLatitudes().length && (!pos1B || !pos2B);e++)
				{
					int posActual1=darSector(actual.darLatitudes()[e], actual.darLongitudes()[e]);

					if(posActual1==pos1)
						pos1B=true;
					if(posActual1==pos2)
						pos2B=true;
					if(pos1B && pos2B)
						respuesta.add(actual);
				}
			}
		}
		if(sectorizacion.get(pos2)!=null && !sectorizacion.get(pos2).cicloRutas.isEmpty())
		{
			for(VORoute actual : sectorizacion.get(pos2).cicloRutas)
			{
				pos1B = false;
				pos2B = false;
				for(int e=0; e<actual.darLatitudes().length && (!pos1B || !pos2B);e++)
				{
					int posActual2=darSector(actual.darLatitudes()[e],actual.darLongitudes()[e]);
					
					if(posActual2==pos1)
						pos1B=true;
					if(posActual2==pos2)
						pos2B=true;
					if(pos1B && pos2B)
						respuesta.add(actual);
				}
			}
		}
		return respuesta;
	}

}
