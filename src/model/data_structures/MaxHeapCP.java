package model.data_structures;

import java.util.Iterator;

public class MaxHeapCP<T extends Comparable<T>> implements IColaPrioridad<T>{

	/**
	 *  arreglo max heap
	 */
	private T[] lista ;
	
	/**
	 * Nodo que representa la cantidad de elementos de la cola.
	 */
	private int cantidadElementos;
	/**
	 * Nodo que representa la maxima cantidad de elementos de la cola.
	 */
	private int maxElements;
	/**
	 * Nodo que representa el primer elemento de la cola.
	 */
	private NodoDoble<T> primero;

	/**
	 * Nodo que representa el ultimo elemento de la cola.
	 */
	private NodoDoble<T> ultimo;
	
	public MaxHeapCP(int pMax)
	{
		primero = null;
		ultimo = null;
		cantidadElementos = 0;
		maxElements = pMax;
		lista = (T[]) new Comparable[pMax];
	}
	
	public int darHijoIzquierda(int iPadre)
	{
		return 2*iPadre + 1;
	}
	
	public int darHijoDerecha(int iPadre)
	{
		return 2*iPadre + 2;
	}
	
	public int darIndicePadre(int hijoP)
	{
		return (hijoP-1)/2;
	}
	
	public boolean tienePorLaIzquierda(int index)
	{
		return (darHijoIzquierda(index)<cantidadElementos);
	}
	
	public boolean tienePorLaDerecha(int index)
	{
		return darHijoDerecha(index)<cantidadElementos;
	}
	
	public boolean tienePadre(int index)
	{
		return darIndicePadre(index)>=0;
	}
	
	public T hijoIzquierda(int index)
	{
		return (T) lista[darHijoIzquierda(index)];
	}
	
	public T hijoDerecha(int index)
	{
		return (T) lista[darHijoDerecha(index)];
	}
	
	public T parent(int index)
	{
		return (T)lista[darIndicePadre(index)];
	}
	
	public void swap(int index1, int index2)
	{
		T temp = lista[index1];
		lista[index1] = lista[index2];
		lista[index2] = temp;
	}
	
	public void reviseAbajo()
	{
		int index = 0;
		while(tienePorLaIzquierda(index))
		{
			int l1 = darHijoIzquierda(index);
			int verif = hijoDerecha(index).compareTo(hijoIzquierda(index));
			if(tienePorLaIzquierda(index) && verif>0 )
			{
				l1 = darHijoDerecha(index);
			}
			int verif2 = lista[index].compareTo(lista[l1]);
			if(verif>0)
			{
				break;
			}
			else
			{
				swap(index, l1);
			}
			index = l1;
		}
	}
	
	public void reviseArriba()
	{
		int index = cantidadElementos - 1;
		int verif = parent(index).compareTo(lista[index]);
		while(tienePadre(index)&& verif<0)
		{
			swap(darIndicePadre(index),index);
			index = darIndicePadre(index);
		}
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

	@Override
	public void agregar(T elemento) {
		// TODO Auto-generated method stub
		lista[cantidadElementos] = elemento;
		cantidadElementos++;
		reviseArriba();
	}

	@Override
	public T max() throws Exception {
		// TODO Auto-generated method stub
		if(cantidadElementos>0)
		{
			T item = lista[0];
			lista[0] = lista[cantidadElementos-1];
			cantidadElementos--;
			reviseAbajo();
			return item;
		}
		else
		{
			throw new Exception();
		}
		
	}
	

	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return (cantidadElementos == 0)? true: false;
	}

	@Override
	public int tamanoMax() {
		// TODO Auto-generated method stub
		return maxElements;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorListaDoble<T>(primero);
	}

	@Override
	public T first() {
		// TODO Auto-generated method stub
		return primero.darElemento();
	}

	@Override
	public T last() {
		// TODO Auto-generated method stub
		return ultimo.darElemento();
	}

}
