package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Lista doblemente encadenada
 * @author Luis Miguel Gomez Londono
 *
 * @param <T>
 */
public class DoubleLinkedList <T> implements IDoubleLinkedList<T> {

	//------------------------------
	//ATRIBUTOS
	//------------------------------
	/**
	 * Atributo que representa la cantidad de elementos que tiene la lista.
	 */
	private int cantidadElementos;

	/**
	 * Nodo que representa el primer nodo de la lista.
	 */
	private NodoDoble<T> primerNodo;

	//------------------------------
	//CONSTRUCTORES
	//------------------------------

	/**
	 * Constructor para una lista vacia...
	 */
	public DoubleLinkedList()
	{
		cantidadElementos = 0;
		primerNodo = null;
	}

	/**
	 * Constructor para crear una lista apartir de un elemento.
	 * @param pElemento el primer elemento que va a tener la nueva lista.
	 */
	public DoubleLinkedList(T pElemento)
	{
		if(pElemento == null)
		{
			throw new NullPointerException("Se esta recibiendo un elemento nulo");
		}
		primerNodo = new NodoDoble<T>(pElemento);
		cantidadElementos = 1;

	}

	//------------------------------
	//METODOS
	//------------------------------

	/**
	 * Metodo que devuelve el iterador de una lista muy sencilla. 
	 */
	public Iterator<T> iterator() {

		return new IteradorListaDoble<T>(primerNodo);
	}
	@Override
	public Integer getSize() {

		return cantidadElementos;
	}

	@Override
	public boolean add(T pElemento) {

		NodoDoble<T> actual = primerNodo;
		NodoDoble<T> nuevo = new NodoDoble<T>(pElemento);
		boolean seAgrego = false;

		if(pElemento != null)
		{

			if(actual == null)
			{
				primerNodo = nuevo;
				cantidadElementos ++;
				seAgrego = true;
			}
			else
			{
				primerNodo = nuevo;
				primerNodo.cambiarSiguiente(actual);
				actual.cambiarAnterior(primerNodo);
				cantidadElementos++;
				seAgrego = true;
			}

		}

		return seAgrego;

	}
	
	/**
     * Agrega un elemento a la lista manteniendo el orden de acuerdo al criterio de comparación, actualiza el número de elementos.
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public boolean addOrdenado(T elemento, boolean ascendente, Comparator<T> criterioC) 
	{
		//TODO Completar según la documentación.
		NodoDoble<T> actual = primerNodo;
		NodoDoble<T> anterior = null;
		NodoDoble<T> nuevo = new NodoDoble<T>(elemento);
		boolean agrego = false;
		Comparator<T> comparador = criterioC;
		/**
		 * RESUMEN PARA VER SI ENTENDI: ascendente es -1 porque queremos que la comparacion
		 * nos devuelva -1 porque SOLAMENTE hago cambios si el objeto que quiero meter es mayor
		 * que el elemento actual, o sea que quedaria ascendente.
		 * Para desdendente lo mismo pero con 1, el elemento que quiero meter es mayor que el 
		 * actual.
		 */
		int comandoAscendente = -1;
		//verificar si es descendente
		if(!ascendente)
		{
			comandoAscendente = 1;
		}
		//verificamos que el elemento no sea null
		if(elemento == null)
			throw new NullPointerException("El elemento que me llega es nulo");
		else
		{
				//lista vacia?
				if(actual == null)
				{
					primerNodo = nuevo;
					cantidadElementos++;
					agrego =true;
				}
				//no es vacia, lets sort it out!
				else
				{
					//si y solo si el primer nodo es mayor/menor que el actual entonces evitamos el nullpointer con el anterior
					if(comparador.compare(elemento, actual.darElemento()) == comandoAscendente)
					{
						primerNodo = nuevo;
						primerNodo.cambiarSiguiente(actual);
						cantidadElementos++;
						agrego = true;
					}
					else
					{						
						//itero mientras se cumpla con el comando y en el que este parado no sea null
						while (actual != null && comparador.compare(elemento, actual.darElemento()) == comandoAscendente);
						{
							anterior = actual;
							actual = actual.darSiguiente();
						}
						anterior.cambiarSiguiente(nuevo);
						nuevo.cambiarSiguiente(actual);
						cantidadElementos++;
						agrego = true;
					}
				}				
		}
		return agrego;
	}

	@Override
	public T getElement(int pPosicion) throws IndexOutOfBoundsException
	{

		NodoDoble<T> actual = primerNodo;

		if(pPosicion < 0 || pPosicion > cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se esta pidiendo la posicion " + pPosicion + "excede el tamanio de la lista...");
		}
		else
		{

			int i = 0;
			while(actual != null && i != pPosicion)
			{
				actual = actual.darSiguiente();
				i++;
			}

		}

		return actual.darElemento();

	}

	public void clearAll()
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

}
