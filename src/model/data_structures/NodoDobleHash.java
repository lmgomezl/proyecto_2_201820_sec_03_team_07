package model.data_structures;

/**
 * Nodo Doble
 * @author Luis Miguel Gomez Londono
 *
 * @param <T>
 */
public class NodoDobleHash<K, V>{

	//------------------------------
	//ATRIBUTOS
	//------------------------------

	/**
	 * Atributo que modela el nodo anterior
	 */
	private NodoDobleHash<K, V> anterior;

	/**
	 * Atributo que modela el nodo siguiente
	 */
	private NodoDobleHash<K, V> siguiente;

	/**
	 * Atributo que modela el valor contenido en el nodo.
	 */
	protected V value;
	
	/**
	 * Atributo que modela la llave del contenido en el nodo.
	 */
	protected K key;

	//------------------------------
	//CONSTRUCTORES
	//------------------------------

	/**
	 * Constructor para un nodo doblemente encadenado.
	 * @param pElemento que va a ser contenido en el nodo. 
	 */
	public NodoDobleHash(K pKey, V pValor) 
	{
		key = pKey;
		value = pValor;
		anterior = null;
		siguiente = null;
	}

	//------------------------------
	//METODOS
	//------------------------------
	
	/**
	 * Metodo que cambia el nodo siguiente
	 * @param pSiguiente el nodo que se va a cambiar por el siguiente. 
	 */
	public void cambiarSiguiente(NodoDobleHash<K, V> pSiguiente)
	{
		siguiente = pSiguiente;
	}

	/**
	 * Metodo que cambia el nodo anterior
	 * @param pAnterior el nodeo que se va a cambiar por el anterior. 
	 */
	public void cambiarAnterior(NodoDobleHash<K,V> pAnterior)
	{
		anterior = pAnterior;
	}

	/**
	 * Metodo que retorna el siguiente elemento de este nodo
	 * @return el nodo siguiente a este
	 */
	public NodoDobleHash<K,V> darSiguiente()
	{
		return siguiente;
	}

	/**
	 * Metodo que retorna el anterior elemento de este nodo. 
	 * @return el nodo anterior a este.
	 */
	public NodoDobleHash<K,V> darAnterior()
	{
		return anterior;
	}

	/**
	 * Metodo que retorna la llave del elemento contenido dentro de este nodo.
	 * @return la llave del elemento contenido en este nodo. 
	 */
	public K darLlave()
	{
		return key;
	}
	
	/**
	 * Metodo que retorna el valor del elemento contenido dentro de este nodo.
	 * @return el vlaor del elemento contenido en este nodo. 
	 */
	public V darValor()
	{
		return value;
	}

	/**
	 * Metodo que cambia el elemento contenido en este nodo. 
	 * @param pKey la llave del elemento que se quiere cambiar en este nodo. 
	 */
	public void cambiarLlave(K pKey)
	{
		key =  pKey;
	}
	
	/**
	 * Metodo que cambia el elemento contenido en este nodo. 
	 * @param pValor el valor del elemento que se quiere cambiar en este nodo. 
	 */
	public void cambiarValor(V pValor)
	{
		value =  pValor;
	}
	
	/**
	 * Metodo que cambia llave y valor del contenido en este nodo. 
	 * @param pValor el valor del elemento que se quiere cambiar en este nodo.  
	 * @param pKey la llave del elemento que se quiere cambiar en este nodo. 
	 */
	public void cambiarElementos(K pKey, V pValor)
	{
		value =  pValor;
		key = pKey;
	}

}
