/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author luisgomez
 *
 */
public interface IColaPrioridad <T extends Comparable<T>> extends Iterable<T>{

	T first();
	
	T last();
	
	int darNumeroElementos();
	
	void agregar(T elemento);
	
	T max() throws Exception;
	
	boolean esVacia();
	
	int tamanoMax();
	
	Iterator<T> iterator();
	
}
