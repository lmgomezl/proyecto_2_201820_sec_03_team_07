package model.data_structures;

/**
 * @author Luis Miguel Gomez Londono
 * Interface Data Type for a doubly-linked list of generic objects to implement in SeparateChainingHash.
 * @param <T>
 */
public interface IHashDoubleLinkedList<K, V> extends Iterable<K> {
	
	/**
	 * Operacion para el tamanio de la lista
	 * @return el tamanio de la lista
	 */
	Integer getSize();
	
	/**
	 * Operacion para agregar un nodo de llave/valor a la lista en la primera posicion. 
	 * @param el elemento que se desea agregar
	 * @return true si se agrego satisfactoriamente, false en el caso contrario.
	 */
	public boolean add(K pKey, V pValor);
	
	/**
	 * Operacion que toma un elemento con cierta llave.
	 * @param pKey la llave de donde queremos extraer el elemento.
	 * @return el objeto
	 */
	public V getElement(K pKey);
	
	/**
	 * Operacion que borra toda la lista
	 */
	public void clearAll();
	
	/**
	 * Operacion que verifica si existe un elemento con esa llave en la lista
	 * @param pKey la llave de la cual queremos verificar existencia.
	 * @return true si existe, false en el caso contrario.
	 */
	public boolean contains(K pKey);

	/**
	 * Operacion que elimina un nodo de la lista basado en su llave
	 * @param pKey la llave de la cual queremos sacar de existencia.
	 * @return V el objeto que se borro.
	 */
	public V eliminar(K pKey);

}
