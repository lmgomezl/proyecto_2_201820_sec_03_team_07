package model.data_structures;
import java.util.Iterator;

/**
 * @author Luis Miguel Gomez Londono
 * Clase que representa el iterador de lista muuuy sencilla (avanza hacia adelante)
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class IteradorListaDoble<E> implements Iterator<E>
{
	
	/**
	 * El nodo donde se encuentra el iterador.
	 */
	private NodoDoble<E> actual;

	
	public IteradorListaDoble(NodoDoble<E> primerNodo) 
	{
		actual = primerNodo;
	}
	
	/**
     * Indica si aún hay elementos por recorrer
     * @return true en caso de que  aún haya elemetos o false en caso contrario
     */
	public boolean hasNext() 
	{
		return actual != null;
	}

	/**
     * Devuelve el siguiente elemento a recorrer
     * <b>post:</b> se actualizado actual al siguiente del actual
     * @return objeto en actual
     */
	public E next() 
	{
		E valor = actual.darElemento();
		actual = actual.darSiguiente();
		return valor;
	}

}
