package model.data_structures;

public class NodoSencillo<T>
{

	/**
	 * Elemento almacenado en el nodo.
	 */
	protected T elemento;

	/**
	 * Siguiente nodo.
	 */
	//TODO Defina el atributo siguiente como el siguiente nodo de la lista.
	private NodoSencillo<T> siguiente;

	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenar� en el nodo. elemento != null
	 */
	public NodoSencillo(T pElemento)
	{
		//TODO Completar de acuerdo a la documentaci�n.
		elemento = pElemento;
		siguiente = null;
	}

	/**
	 * M�todo que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoSencillo<T> pSiguiente)
	{
		//TODO Completar de acuerdo a la documentaci�n.
		siguiente = pSiguiente;
	}

	/**
	 * M�todo que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public T darElemento()
	{
		//TODO Completar de acuerdo a la documentaci�n.
		return elemento;
	}

	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenar� en el nodo.
	 */
	public void cambiarElemento(T pElemento)
	{
		//TODO Completar de acuerdo a la documentaci�n.
		elemento = pElemento;
	}

	/**
	 * M�todo que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoSencillo<T> darSiguiente()
	{
		//TODO Completar de acuerdo a la documentaci�n.
		return siguiente;
	}

}
