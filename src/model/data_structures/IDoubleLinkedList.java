package model.data_structures;

/**
 * @author Luis Miguel Gomez Londono
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoubleLinkedList<T> extends Iterable<T> {
	
	/**
	 * Operacion para el tamanio de la lista
	 * @return el tamanio de la lista
	 */
	Integer getSize();
	
	/**
	 * Operacion para agregar un nodo a la lista en la primera posicion. 
	 * @param el elemento que se desea agregar
	 * @return true si se agrego satisfactoriamente, false en el caso contrario.
	 */
	public boolean add(T pElemento);
	
	/**
	 * Operacion que toma un elemento en cierta posicion
	 * @param pPosicion el numero de donde se quiere obtener un objeto
	 * @return el objeto
	 */
	public T getElement(int pPosicion);
	
	/**
	 * Operacion que borra toda la lista
	 */
	public void clearAll();

}
