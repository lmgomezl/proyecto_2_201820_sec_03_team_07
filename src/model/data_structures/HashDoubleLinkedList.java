package model.data_structures;

import java.util.Iterator;

/**
 * Lista doblemente encadenada
 * @author Luis Miguel Gomez Londono
 *
 * @param <K, V>
 */
public class HashDoubleLinkedList<K, V> implements IHashDoubleLinkedList<K, V> {

	//------------------------------
	//ATRIBUTOS
	//------------------------------
	/**
	 * Atributo que representa la cantidad de elementos que tiene la lista.
	 */
	private int cantidadElementos;

	/**
	 * Nodo que representa el primer nodo de la lista.
	 */
	private NodoDobleHash<K,V> primerNodo;

	//------------------------------
	//CONSTRUCTORES
	//------------------------------

	/**
	 * Constructor para una lista vacia...
	 */
	public HashDoubleLinkedList()
	{
		cantidadElementos = 0;
		primerNodo = null;
	}

	/**
	 * Constructor para crear una lista apartir de un elemento.
	 * @param pElemento el primer elemento que va a tener la nueva lista.
	 */
	public HashDoubleLinkedList(K pKey, V pValor)
	{
		if(pKey == null || pValor == null)
		{
			throw new NullPointerException("Se esta recibiendo un elemento nulo");
		}
		primerNodo = new NodoDobleHash<K,V>(pKey, pValor);
		cantidadElementos = 1;

	}

	//------------------------------
	//METODOS
	//------------------------------

	/**
	 * Metodo que devuelve el iterador de una lista muy sencilla. 
	 */
	public Iterator<K> iterator() {

		return new IteradorListaHash<K,V>(primerNodo);
	}
	@Override
	public Integer getSize() {

		return cantidadElementos;
	}

	@Override
	public boolean add(K pKey, V pValor) {

		NodoDobleHash<K,V> actual = primerNodo;
		NodoDobleHash<K,V> nuevo = new NodoDobleHash<K,V>(pKey, pValor);
		boolean seAgrego = false;

		if(pKey != null || pValor != null)
		{

			if(actual == null)
			{
				primerNodo = nuevo;
				cantidadElementos ++;
				seAgrego = true;
			}
			else
			{
				primerNodo = nuevo;
				primerNodo.cambiarSiguiente(actual);
				actual.cambiarAnterior(primerNodo);
				cantidadElementos++;
				seAgrego = true;
			}

		}
		return seAgrego;

	}

	@Override
	public V getElement(K pKey)
	{

		NodoDobleHash<K,V> actual = primerNodo;
		while(actual != null)
		{
			if(actual.darLlave().equals(pKey))
				return actual.darValor();
			actual = actual.darSiguiente();
		}
		if (actual == null)
			return null;
		return actual.darValor();

	}

	@Override
	public V eliminar(K pKey)
	{
		V borre = null;
		if(contains(pKey))
		{
			NodoDobleHash<K,V> actual = primerNodo;
			while(actual != null)
			{
				if(actual.darLlave().equals(pKey))
					break;
				actual = actual.darSiguiente();
			}
			borre = actual.darValor();
			//si es el primero
			if(actual == primerNodo)
			{
				//si solo hay un elemento
				if(actual.darSiguiente() != null)
				{
					primerNodo = actual.darSiguiente();
					primerNodo.cambiarAnterior(null);					
				}
				else
				{
					primerNodo = null;
				}
			}
			//mi siguiente es null== soy el ultimo.
			else if(actual.darSiguiente() == null)
			{
				actual.darAnterior().cambiarSiguiente(null);
				actual.cambiarAnterior(null);
			}
			else
			{
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				actual.darSiguiente().cambiarAnterior(actual.darAnterior());
				actual.cambiarSiguiente(null);
				actual.cambiarAnterior(null);
			}
		}
		return borre;
	}

	public void clearAll()
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	@Override
	public boolean contains(K pKey) {
		return getElement(pKey) != null;
	}

}
