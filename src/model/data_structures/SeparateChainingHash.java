/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author luisgomez
 * @param <V>
 *
 */
public class SeparateChainingHash<K extends Comparable<K>, V> implements IHashTable<K, V> {

	private int numeroElementos;

	private int capacity;

	/**
	 * Atributo que representa un arreglo de listas encadenadas... cada posicion en el arreglo tiene una lista encadenada dentro de la misma.
	 */
	private IHashDoubleLinkedList<K, V>[] listaEncadenada;

	/**
	 * Metodo constructor de la Tabla Hash usando los metodos de LINEAR PROBING.
	 * @param m capacidad minima de elementos que queremos tener.
	 */
	@SuppressWarnings("unchecked")
	public SeparateChainingHash(int m) {
		capacity = m;
		numeroElementos = 0;
		listaEncadenada = (IHashDoubleLinkedList<K, V>[]) new HashDoubleLinkedList[m];
		for(int i = 0; i < m; i++)
		{
			listaEncadenada[i] = new HashDoubleLinkedList<K,V>();
		}
	}

	@Override
	public int size() {
		return numeroElementos;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public void put(K pKey, V pValue) {
		if(pKey==null){

		}
		if(pValue==null){
			delete(pKey);
			return;	
		}

		int hashPos = hash(pKey);
		int i = hashPos;
		if(!listaEncadenada[i].contains(pKey))
		{
			numeroElementos++;
		}
		listaEncadenada[i].add(pKey, pValue);
	}

	@Override
	public V get(K pKey) {
		return listaEncadenada[hash(pKey)].getElement(pKey);
	}
	public IHashDoubleLinkedList<K, V> getlist(K pKey) {
		return listaEncadenada[hash(pKey)];
	}

	@Override
	public V delete(K pKey) {
		if(pKey==null){
			return null;
		}
		int hashPos = hash(pKey);
		int i = hashPos;
		if(listaEncadenada[i].contains(pKey))
			numeroElementos--;
		return listaEncadenada[i].eliminar(pKey);
	}

	@Override
	public Iterator<K> keys() {
		Queue<K> queue = new Queue<K>();
		for(int i = 0; i < capacity; i++)
		{
			for(K llave: listaEncadenada[i])
			{
				queue.enqueue(llave);
			}
		}
		return queue.iterator();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size()==0;
	}

	@Override
	public boolean existe(K pKey) {
		// TODO Auto-generated method stub
		return get(pKey) != null;
	}

	@Override
	public int hash(K pKey) {
		int hashed = 0;
		double porcentaje = (double) numeroElementos/capacity;
		if(porcentaje < 2.0)
		{
			hashed = (pKey.hashCode() & 0x7fffffff) % capacity;		
		}
		else {
			rehash(capacity*2);
			hashed = (pKey.hashCode() & 0x7fffffff) % capacity;
		}
		return hashed;
	}

	@Override
	public void rehash(int pCapacity)
	{
		SeparateChainingHash<K, V> temp = new SeparateChainingHash<K, V>(pCapacity);
		for(int i = 0; i < capacity; i++)
		{
			for(K llave: listaEncadenada[i])
			{
				temp.put(llave, listaEncadenada[i].getElement(llave));
			}
		}
		listaEncadenada = temp.darLista();
		capacity = pCapacity;
	}

	public IHashDoubleLinkedList<K,V>[] darLista()
	{
		return listaEncadenada;
	}

}
