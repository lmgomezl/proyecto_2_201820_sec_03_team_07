/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author luisgomez
 * Implementacion de un queue con nodos doblemente encadenados.
 */
public class Queue <T> implements IQueue<T> {

	//---------------------------------------------
	// ATRIBUTOS
	//---------------------------------------------

	/**
	 * Nodo que representa el primer elemento de la cola.
	 */
	private NodoDoble<T> primero;

	/**
	 * Nodo que representa el ultimo elemento de la cola.
	 */
	private NodoDoble<T> ultimo;

	/**
	 * Atributo que representa la cantidad de elementos de la lista.
	 */
	private int cantidadElementos;

	//---------------------------------------------
	// CONSTRUCTORES
	//---------------------------------------------

	/**
	 * Construye un Queue a base de una lista doblemente encadenada.
	 */
	public Queue ()
	{
		primero = null;
		ultimo = null;
		cantidadElementos = 0;
	}

	//---------------------------------------------
	// METODOS
	//---------------------------------------------

	/**
	 * Metodo que devuelve el iterador de esta lista.
	 * @return un iterador de la lista
	 */
	public Iterator<T> iterator() {
		return new IteradorListaDoble<T>(primero);
	}

	/**
	 * Metodo que calcula si una lista esta vacia.
	 * @return true si esta vacia, false en el caso contrario. 
	 */
	public boolean isEmpty() {
		return (cantidadElementos == 0)? true: false;
	}

	/**
	 * Metodo que calcula el tamanio de la lista.
	 * @return el tamanio de la lista.
	 */
	public int size() {
		return cantidadElementos;
	}

	/**
	 * Metodo que agrega un nuevo elemento a la cola, entra en la ultima posicion. 
	 */
	public void enqueue(T t) {
		NodoDoble<T> nuevo = new NodoDoble<T>(t);
		if(isEmpty())
		{
			primero = nuevo;
			ultimo = primero;
			cantidadElementos++;
		}
		else
		{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
			ultimo = nuevo;
			cantidadElementos++;
		}


	}

	/**
	 * Metodo que saca un elemento de la cola, se saca el primer nodo de la cola. 
	 */
	public T dequeue() {
		//AQU� ES DONDE TENGO LA DUDA
		T teBote=null;
		if(primero!=null)
		{
			teBote = primero.darElemento();
		}
		if(cantidadElementos < 2)
		{
			primero = null;
			ultimo = primero;
			cantidadElementos--;
		}
		else
		{
			NodoDoble<T> nuevoPrimero = primero.darSiguiente();
			primero = nuevoPrimero;
			primero.cambiarAnterior(null);
			cantidadElementos--;
		}

		return teBote;
	}

	/**
	 * Metodo que retorna el primer nodo de la cola.
	 * @return el primer nodo de la cola. 
	 */
	public NodoDoble<T> darPrimero()
	{
		return primero;
	}

	/**
	 * Metodo que retorna el ultimo nodo de la cola.
	 * @return el ultimo nodo de la cola. 
	 */
	public NodoDoble<T> darUltimo()
	{
		return ultimo;
	}

}
