package model.data_structures;

import java.util.Iterator;

/**
 * @author luisgomez
 *
 */
public class ColaPrioridad<T extends Comparable<T>> implements IColaPrioridad<T>{

	/**
	 * Nodo que representa la cantidad de elementos de la cola.
	 */
	private int cantidadElementos;
	/**
	 * Nodo que representa la maxima cantidad de elementos de la cola.
	 */
	private int maxElements;
	/**
	 * Nodo que representa el primer elemento de la cola.
	 */
	private NodoDoble<T> primero;

	/**
	 * Nodo que representa el ultimo elemento de la cola.
	 */
	private NodoDoble<T> ultimo;

	public ColaPrioridad(int pMax)
	{
		primero = null;
		ultimo = null;
		cantidadElementos = 0;
		maxElements = pMax;

	}

	@Override
	public int tamanoMax()
	{
		return maxElements;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorListaDoble<T>(primero);
	}

	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return (cantidadElementos == 0)? true: false;	
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

	@Override
	public void agregar(T t) {
		NodoDoble<T> nuevo = new NodoDoble<T>(t);
		if(cantidadElementos <= maxElements)
		{
			if(esVacia())
			{
				primero = nuevo;
				ultimo = primero;
				cantidadElementos++;
			}
			else
			{
				ultimo.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(ultimo);
				ultimo = nuevo;
				cantidadElementos++;
			}
		}
		else
		{
			System.out.println("no se pueden agregar mas elementos, se definio un tamanio maximo de: " + maxElements);
			return;
		}

	}

	@Override
	public T max() {
		NodoDoble<T> sacar = null;
		if(cantidadElementos > 1)
		{
			NodoDoble<T> actual = primero;
			T mayor = actual.darElemento();
			int i = 0;
			int posicionMayor = 0;
			while(actual != null)
			{
				if(actual.darElemento().compareTo(mayor) >= 0) {
					mayor = actual.darElemento();
					posicionMayor = i;
				}
				actual = actual.darSiguiente();
				i++;
			}
			//es el primero
			if(posicionMayor == 0) {
				sacar = primero;
				if(cantidadElementos > 1) {
					primero = primero.darSiguiente();
					primero.cambiarAnterior(null);
				}
				else
				{
					primero = null;
				}
			}
			//es el ultimo
			else if(posicionMayor == cantidadElementos-1)
			{
				sacar = ultimo;
				ultimo = ultimo.darAnterior();
				ultimo.cambiarSiguiente(null);
			}
			//in between
			else
			{
				int j =0;
				actual = primero;
				NodoDoble<T> anterior = null;
				NodoDoble<T> siguiente = null;
				//Solo avanzamos y dejamos todo acomodado
				while(actual.darSiguiente()!=null && j < posicionMayor)
				{
					actual = actual.darSiguiente();
					anterior = actual.darAnterior();
					siguiente = actual.darSiguiente();
					j++;
				}
				anterior.cambiarSiguiente(siguiente);
				siguiente.cambiarAnterior(anterior);
				actual.cambiarSiguiente(null);
				actual.cambiarAnterior(null);
				sacar = actual;
			}
		}
		else if(cantidadElementos == 1)
		{
			sacar = primero;
			primero = null;
		}
		else
		{
			System.out.println("No hay mas elementos para sacar de la lista...");
			return null;
		}
		cantidadElementos--;
		return sacar.darElemento();
	}

	@Override
	public T first() {
		// TODO Auto-generated method stub
		return primero.darElemento();
	}

	@Override
	public T last() {
		// TODO Auto-generated method stub
		return ultimo.darElemento();
	}

}
