package model.data_structures;

import java.util.Comparator;

/*
 * Lista linked ordenada
 */
public class LinkedListOrdered<E> extends LinkedList<E>
{

	/**
	 * Comparador de E, permite comparar dos E para mantener el orden en la lista.
	 */
	private Comparator<E> comparador;

	private boolean ascendente;

	/**
	 * Construye una lista vac�a.
	 * <b>post: </b> - Se ha inicializado el primer nodo en null. <br/>
	 * - Se ha inicializado el criterio de comparaci�n por el que se ordenar� el elemento.
	 * - Se ha inicializado si se quiere ordenar ascendente.
	 * @param comparador Criterio de comparaci�n por el que se ordenar�n los elementos en la lista.
	 */
	public LinkedListOrdered(Comparator<E> comparador, boolean ascendente)
	{
		this.comparador = comparador;
		this.ascendente = ascendente;
	}

	/**
	 * Metodo que decide si se ordena ascendente o descendente con respecto a lo estipulado en el constructor.
	 * @param E elemento que se desea agregar...
	 * @return true si se agrego, false en el caso contrario.
	 */
	public boolean add(E elemento)
	{
		if(ascendente)
			return addAscendente(elemento);
		else
			return addDescendente(elemento);

	}

	/**
	 * Agrega un elemento a la lista manteniendo el orden Descendente de acuerdo al criterio de comparaci�n, actualiza el n�mero de elementos.
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	private boolean addDescendente(E elemento) {
		NodoSencillo<E> actual = primerNodo;
		NodoSencillo<E> anterior = null;
		NodoSencillo<E> nuevo = new NodoSencillo<E>(elemento);
		boolean agrego = false;

		if(primerNodo == null)
		{
			primerNodo = nuevo;
			agrego = true;
			cantidadElementos++;
		}
		else
		{

			while(actual!=null && !agrego)
			{
				if(actual.darSiguiente() != null)
				{
					if(comparador.compare(elemento, actual.darElemento()) >= 0)
					{
						if(anterior==null)
						{
							primerNodo = nuevo;
							primerNodo.cambiarSiguiente(actual);
							agrego = true;
							cantidadElementos++;
						}
						else
						{
							anterior.cambiarSiguiente(nuevo);
							nuevo.cambiarSiguiente(actual);
							agrego =true;
							cantidadElementos++;
						}
					}
					else 
					{		
						anterior = actual;
						actual = actual.darSiguiente();	
					}
				}
				else
				{
					if(anterior==null)
					{

						if(comparador.compare(nuevo.darElemento(), actual.darElemento()) >= 0)
						{
							primerNodo = nuevo;
							primerNodo.cambiarSiguiente(actual);
							agrego=true;
							cantidadElementos++;														
						}
						else
						{
							primerNodo.cambiarSiguiente(nuevo);
							agrego=true;
							cantidadElementos++;
						}

					}
					else
					{
						actual.cambiarSiguiente(nuevo);
						agrego =true;
						cantidadElementos++;						
					}
				}

			}

		}

		return agrego;		
	}

	/**
	 * Agrega un elemento a la lista manteniendo el orden Ascendente de acuerdo al criterio de comparaci�n, actualiza el n�mero de elementos.
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean addAscendente(E elemento) 
	{
		NodoSencillo<E> actual = primerNodo;
		NodoSencillo<E> anterior = null;
		NodoSencillo<E> nuevo = new NodoSencillo<E>(elemento);
		boolean agrego = false;

		if(primerNodo == null)
		{
			primerNodo = nuevo;
			agrego = true;
			cantidadElementos++;
		}
		else
		{

			while(actual!=null && !agrego)
			{
				if(actual.darSiguiente() != null)
				{
					if(comparador.compare(elemento, actual.darElemento()) > 0)
					{
						anterior = actual;
						actual = actual.darSiguiente();
						if(actual.darSiguiente() == null)
						{
							actual.cambiarSiguiente(nuevo);
							agrego = true;
							cantidadElementos++;

						}
					}
					else {
						if(anterior == null)
						{
							primerNodo = nuevo;
							primerNodo.cambiarSiguiente(actual);
							agrego = true;
							cantidadElementos++;
						}
						else
						{
							anterior.cambiarSiguiente(nuevo);
							nuevo.cambiarSiguiente(actual);
							agrego=true;
							cantidadElementos++;
						}

					}
				}
				else
				{
					actual.cambiarSiguiente(nuevo);
					agrego=true;
					cantidadElementos++;

				}

			}

		}

		return agrego;
	}

}

