/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author luisgomez
 * @param <V>
 *
 */
public class LinearProbingHash<K, V> implements IHashTable<K, V> {

	private int numeroElementos;

	private int capacity;

	private V[] values;

	private K[] keys;

	/**
	 * Metodo constructor de la Tabla Hash usando los metodos de LINEAR PROBING.
	 * @param m capacidad minima de elementos que queremos tener.
	 */
	@SuppressWarnings("unchecked")
	public LinearProbingHash(int m) {
		capacity = m;
		numeroElementos = 0;
		values = (V[]) new Object[m];
		keys = (K[]) new Object[m];
	}

	public V[] getValues()
	{
		return values;
	}

	public K[] getKeys()
	{
		return keys;
	}

	@Override
	public int size() {
		return numeroElementos;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public void put(K pKey, V pValue) {
		int hashPos = hash(pKey);
		int i;
		for(i = hashPos; keys[i] != null; i = (i+1) % capacity)
		{
			//si ya existe uno con la misma llave le reemplazamos el valor
			if(keys[i].equals(pKey))
			{
				values[i] = pValue;
				return;
			}

		}
		keys[i] = pKey;
		values[i] = pValue;
		numeroElementos++;
	}

	@Override
	public V get(K pKey) {
		int hashPos = hash(pKey);
		for(int i = hashPos; keys[i] != null; i = (i+1) % capacity)
		{
			if(keys[i].equals(pKey))
				return values[i];
		}
		return null;
	}

	@Override
	public V delete(K pKey) {
		// TODO Auto-generated method stub
		V valorBorrado = null;
		if(!existe(pKey))
			return valorBorrado;
		int hashPos = hash(pKey);
		//Recorremos las llaves y recorrer mientras no exista...
		while(!keys[hashPos].equals(pKey))
		{
			hashPos++;
		}
		valorBorrado = values[hashPos];
		keys[hashPos] = null;
		values[hashPos] = null;
		numeroElementos--;
		return valorBorrado;
	}

	@Override
	public Iterator<K> keys() {
		Queue<K> queue = new Queue<K>();
		for(int i = 0; i < capacity; i++)
		{
			if(keys[i] != null)
				queue.enqueue(keys[i]);
		}
		return queue.iterator();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size()==0;
	}

	@Override
	public boolean existe(K pKey) {
		// TODO Auto-generated method stub
		return get(pKey) != null;
	}

	@Override
	public int hash(K pKey) {
		int hashed = 0;
		double porcentaje = (double) numeroElementos/capacity;
		if(porcentaje < 0.75)
		{
			hashed = (pKey.hashCode() & 0x7fffffff) % capacity;		
		}
		else {
			rehash(capacity*2);
			hashed = (pKey.hashCode() & 0x7fffffff) % capacity;
		}
		return hashed;
	}

	@Override
	public void rehash(int pCapacity)
	{
		LinearProbingHash<K, V> temp = new LinearProbingHash<K, V>(pCapacity);
		for(int i = 0; i < capacity; i++)
		{
			if(keys[i] != null)
				temp.put(keys[i], values[i]);
		}
		values = temp.getValues();
		keys = temp.getKeys();
		capacity = pCapacity;
	}

}
