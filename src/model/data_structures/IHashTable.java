/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author luisgomez
 * @param <K>
 *
 */
public interface IHashTable<K, V> {

	/**
	 * Meotodo que retorna la cantidad de elementos en la tabla.
	 * @return la cantidad de elementos.
	 */
	public int size();

	/**
	 * Metodo que retorna la capacidad que tiene la tabla.
	 * @return la capacidad del arreglo.
	 */
	public int capacity();

	/**
	 * Agregar una dupla (K, V) a la tabla. Si la llave K
	 *existe, se reemplaza su valor V asociado.
	 * @param pKey la llave con la que se va a asociar.
	 * @param pValue != null
	 */
	public void put(K pKey, V pValue);

	/**
	 * Obtener el valor V asociado a la llave K.
	 * @param pKey
	 * @return V != null
	 */
	public V get(K pKey);

	/**
	 * Borrar la dupla asociada a la llave K. Se obtiene el
	 *valor V asociado a la llave K. Se obtiene null si la
	 *llave K no existe.
	 * @param pKey
	 * @return
	 */
	public V delete(K pKey);

	/**
	 * Conjunto de llaves K presentes en la tabla.
	 * @return el iterador de llaves en la tabla.
	 */
	public Iterator<K> keys();

	/**
	 * Metodo que describe si el arreglo esta vacio de elementos.
	 * @return true si esta vacio, false en el caso contrario.
	 */
	public boolean isEmpty();

	/**
	 * Verifica la existencia de esa llave en el arreglo.
	 * @param pKey la llave que queremos verificar. != null.
	 * @return true si existe, false en el caso contrario.
	 */
	public boolean existe(K pKey);

	/**
	 * Calcula el codigo hash que se va a usar para esa llave...
	 * @param pKey la llave a la que queremos aplicarle hash segun la funcion de la implementacion
	 * @return el codigo hash para guardar luego en el arreglo...
	 */
	public int hash(K pKey);

	/**
	 * Agranda/achiquita la capacidad del arreglo por el numero que entre por parametro
	 * @param pNuevo tamanio
	 */
	public void rehash(int pNuevo);

}
