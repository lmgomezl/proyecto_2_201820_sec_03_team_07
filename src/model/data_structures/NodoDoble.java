package model.data_structures;

import java.util.Comparator;

/**
 * Nodo Doble
 * @author Luis Miguel Gomez Londono
 *
 * @param <T>
 */
public class NodoDoble<T>{

	//------------------------------
	//ATRIBUTOS
	//------------------------------

	/**
	 * Atributo que modela el nodo anterior
	 */
	private NodoDoble<T> anterior;

	/**
	 * Atributo que modela el nodo siguiente
	 */
	private NodoDoble<T> siguiente;

	/**
	 * Atributo que modela el elemento contenido en el nodo.
	 */
	protected T elemento;

	//------------------------------
	//CONSTRUCTORES
	//------------------------------

	/**
	 * Constructor para un nodo doblemente encadenado.
	 * @param pElemento que va a ser contenido en el nodo. 
	 */
	public NodoDoble(T pElemento) 
	{
		elemento = pElemento;
		anterior = null;
		siguiente = null;
	}

	//------------------------------
	//METODOS
	//------------------------------
	
	/**
	 * Metodo que cambia el nodo siguiente
	 * @param pSiguiente el nodo que se va a cambiar por el siguiente. 
	 */
	public void cambiarSiguiente(NodoDoble<T> pSiguiente)
	{
		siguiente = pSiguiente;
	}

	/**
	 * Metodo que cambia el nodo anterior
	 * @param pAnterior el nodeo que se va a cambiar por el anterior. 
	 */
	public void cambiarAnterior(NodoDoble<T> pAnterior)
	{
		anterior = pAnterior;
	}

	/**
	 * Metodo que retorna el siguiente elemento de este nodo
	 * @return el nodo siguiente a este
	 */
	public NodoDoble<T> darSiguiente()
	{
		return siguiente;
	}

	/**
	 * Metodo que retorna el anterior elemento de este nodo. 
	 * @return el nodo anterior a este.
	 */
	public NodoDoble<T> darAnterior()
	{
		return anterior;
	}

	/**
	 * Metodo que retorna el elemento contenido dentro de este nodo.
	 * @return el elemento contenido en este nodo. 
	 */
	public T darElemento()
	{
		return elemento;
	}

	/**
	 * Metodo que cambia el elemento contenido en este nodo. 
	 * @param pNuevo el elemento que se quiere cambiar en este nodo. 
	 */
	public void cambiarElemento(T pNuevo)
	{
		elemento =  pNuevo;
	}

}
