/**
 * 
 */
package model.data_structures;

/**
 * @author luisgomez
 *
 */
public class NodoRedBlack<K, V> {

	private K llave;
	private V valor;
	private NodoRedBlack<K, V> izq;
	private NodoRedBlack<K, V> der;
	//rojo true, negro false...
	private boolean color;

	public NodoRedBlack (K pLlave, V pValor, boolean pColor) {

		llave = pLlave;
		valor = pValor;
		color = pColor;
	}

	/**
	 * @return the llave
	 */
	public K getKey() {
		return llave;
	}

	/**
	 * @param llave the llave to set
	 */
	public void changeKey(K llave) {
		this.llave = llave;
	}

	/**
	 * @return the valor
	 */
	public V getValue() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValue(V valor) {
		this.valor = valor;
	}

	/**
	 * @return the izq
	 */
	public NodoRedBlack<K, V> getLeft() {
		return izq;
	}

	/**
	 * @param izq the izq to set
	 */
	public void setLeft(NodoRedBlack<K, V> izq) {
		this.izq = izq;
	}

	/**
	 * @return the der
	 */
	public NodoRedBlack<K, V> getRight() {
		return der;
	}

	/**
	 * @param der the der to set
	 */
	public void setRight(NodoRedBlack<K, V> der) {
		this.der = der;
	}

	public boolean getColor()
	{
		return color;
	}
	
	/**
	 * @param color the color to set
	 */
	public void cambiarColor(boolean color) {
		this.color = color;
	}
}
