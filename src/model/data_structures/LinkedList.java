package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements Iterable<T>
{

	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	protected int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	protected NodoSencillo<T> primerNodo;

	/**
	 * Construye la lista vac�a.
	 * <b>post: </b> Se ha inicializado el primer nodo en null
	 */
	public LinkedList() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Agrega un elemento al comienzo de la lista, actualiza el n�mero de elementos.
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 */
	public boolean add(T elemento) 
	{
		// TODO Completar seg�n la documentaci�n
		NodoSencillo<T> actual = primerNodo;
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elemento);
		boolean agrego = false;
				//si no existe un primer nodo se asigna el elemento como el primero
				if(actual == null)
				{					
						primerNodo = nuevo;
						cantidadElementos++;
						agrego = true;
				}
				else
				{
					primerNodo = nuevo;
					primerNodo.cambiarSiguiente(actual);
					cantidadElementos++;
					agrego = true;
					
				}
		
		return agrego;
	}

	public int size()
	{
		return cantidadElementos;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorListaSencilla<T>(primerNodo);
	}

}
